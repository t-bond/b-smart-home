package hu.t_bond.homeassistant.services

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.entities.Entity

class ServiceToggle(private val entityID: String) : Service() {
    override val type = "toggle"
    override val domain = Entity.getDomain(entityID)

    constructor(entity: Entity) : this(entity.entityID)

    override fun getData() = JsonObject().apply {
        addProperty("entity_id", entityID)
    }
}
