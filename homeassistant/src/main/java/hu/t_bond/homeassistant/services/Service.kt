package hu.t_bond.homeassistant.services

import com.google.gson.JsonObject

abstract class Service {
    abstract val domain: String
    abstract val type: String

    open fun getData(): JsonObject? = null
}
