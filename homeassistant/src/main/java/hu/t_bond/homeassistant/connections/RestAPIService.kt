package hu.t_bond.homeassistant.connections

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import hu.t_bond.homeassistant.requests.RefreshAccessToken
import retrofit2.Call
import retrofit2.http.*

internal interface RestAPIService {

    @get:GET("api/")
    val isConnected: Call<JsonObject>

    @get:GET("api/discovery_info")
    val discoveryInfo: Call<JsonObject>

    @get:GET("api/error_log")
    val errors: Call<String>

    @get:GET("api/config/core/check_config")
    val configStatus: Call<JsonObject>

    @get:GET("api/events")
    val events: Call<JsonArray>

    @GET("api/history/period/{start_time}")
    fun getHistory(@Path("start_time") startTime: String?, @Query("filter_entity_id") filterEntityID: String?, @Query("end_time") endTime: String?): Call<JsonArray>

    @GET("api/states/{entity_id}")
    fun getState(@Path("entity_id") entityID: String): Call<JsonObject>

    @POST("api/states/{entity_id}")
    fun updateEntity(@Path("entity_id") entityID: String, @Body newState: JsonObject): Call<JsonObject>

    @DELETE("api/config/automation/config/{automation_id}")
    fun deleteAutomation(@Path("automation_id") automationID: String): Call<JsonObject>

    @POST("api/config/automation/config/{automation_id}")
    fun updateAutomation(@Path("automation_id") automationID: String, @Body automationData: JsonObject): Call<JsonObject>

    @GET("api/config/automation/config/{automation_id}")
    fun getAutomation(@Path("automation_id") automationID: String): Call<JsonObject>

    @FormUrlEncoded
    @POST("auth/token")
    fun revokeRefreshToken(@Field("token") refreshToken: String, @Field("action") IGNORE_ME: String = "revoke"): Call<Unit> //https://github.com/square/retrofit/issues/951

    @FormUrlEncoded
    @POST("auth/token")
    @Headers("${RestAPIConnection.HEADER_REQUEST_TYPE}: ${RefreshAccessToken.REFRESH_TOKEN_TAG}")
    fun authorizationCodeToRefreshToken(@Field("client_id") clientID: String, @Field("code") authorizationCode: String, @Field("grant_type") IGNORE_ME: String = "authorization_code"): Call<JsonObject>

    @FormUrlEncoded
    @POST("auth/token")
    fun renewAccessToken(@Field("client_id") clientID: String, @Field("refresh_token") refreshToken: String, @Field("grant_type") IGNORE_ME: String = "refresh_token"): Call<JsonObject>
}
