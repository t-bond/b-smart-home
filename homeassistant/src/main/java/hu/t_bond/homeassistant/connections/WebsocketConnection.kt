package hu.t_bond.homeassistant.connections

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import hu.t_bond.homeassistant.listeners.InteractionListener
import hu.t_bond.homeassistant.listeners.ResponseListener
import hu.t_bond.homeassistant.model.Session
import hu.t_bond.homeassistant.requests.Authenticate
import hu.t_bond.homeassistant.requests.Ping
import hu.t_bond.homeassistant.requests.Subscription
import hu.t_bond.homeassistant.requests.WebsocketApiRequest
import okhttp3.*
import java.util.*
import java.util.concurrent.locks.ReentrantLock

internal class WebsocketConnection(private val session: Session, baseURL: String, private val interactionListener: InteractionListener): WebSocketListener() {

    companion object {
        const val ENDPOINT_PATH = "api/websocket"

        const val JSON_MESSAGE_TYPE = "type"
        const val JSON_TYPE_PONG = "pong"
        const val JSON_TYPE_AUTH_OK = "auth_ok"
        const val JSON_TYPE_AUTH_INVALID = "auth_invalid"
        const val JSON_TYPE_AUTH_REQUIRED = "auth_required"
        const val JSON_TYPE_EVENT = "event"
        const val JSON_TYPE_RESULT = "result"
        const val JSON_MESSAGE_ID = "id"
        const val JSON_MESSAGE_SUCCESS = "success"

        private const val NORMAL_CLOSURE_STATUS = 1000
        private const val PING_INTERVAL : Long = 0
    }

    private var nextID: Int = 1
        get() = field++

    private var authenticated = false
    private var pendingAuthentication = false
    var connected = false
        private set

    private val queueNeeded get() = !connected || !authenticated || session.expired

    private val websocketEndpoint: String

    private val client = OkHttpClient()
    private lateinit var webSocket: WebSocket

    private val pingTimer = Timer()
    private val pendingSubscriptions = HashMap<Subscription, SubscriptionStatus>()
    private val apiRequests = HashMap<Int, WebsocketApiRequest<*>>()
    private val pendingRequests = ArrayList<WebsocketApiRequest<*>>()
    private val pendingLock = ReentrantLock(true)
    private val requestLock = ReentrantLock(true)
    private val subscriptionLock = ReentrantLock(true)
    private val subscriptions = HashMap<Subscription, Boolean>()

    private val onTokenRefreshed = object : ResponseListener<Boolean>() {
        override fun onResult(result: Boolean) {
            if(result) {
                if(!authenticated)
                    authenticate()
                else{
                    handlePendingRequest()
                    handlePendingSubscriptions()
                }
            }else
                interactionListener.onInvalidSession()
        }

    }

    init {
        websocketEndpoint = baseURL.replace("https://", "wss://")
                .replace("http://", "ws://")
                .plus(ENDPOINT_PATH)
        connect()
    }

    override fun onMessage(webSocket: WebSocket?, text: String?) {
        val response: JsonObject
        val messageType: String
        try {
            response = JsonParser().parse(text!!)!!.asJsonObject
            messageType = response.get(JSON_MESSAGE_TYPE)!!.asString
        } catch (ignored: Exception) {
            return
        }

        val responseID = try {
            response.get("id")?.asInt
        } catch (ignored: Exception) {}

        val matchedResponse : WebsocketApiRequest<*>?
        val matchedSubscription : Subscription?
        if(responseID != null) {
            matchedResponse = apiRequests[responseID]
            matchedSubscription = subscriptions.keys.find { it.id == responseID }
        }else{
            matchedResponse = null
            matchedSubscription = null
        }

        when (messageType) {
            JSON_TYPE_AUTH_REQUIRED -> {
                pendingAuthentication = false
                authenticate()
            }
            JSON_TYPE_AUTH_OK -> {
                authenticated = true
                pendingAuthentication = false
                handlePendingRequest()
                handlePendingSubscriptions()
            }
            JSON_TYPE_AUTH_INVALID -> {
                authenticated = false
                pendingAuthentication = false

                if(session.expired)
                    refreshToken()
            }
            JSON_TYPE_PONG -> {}

            // With ID
            JSON_TYPE_EVENT -> {
                try {
                    matchedSubscription?.onResponse(response.get(JSON_TYPE_EVENT).asJsonObject)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            JSON_TYPE_RESULT -> {
                if(matchedResponse != null) {
                    matchedResponse.onMessage(this.webSocket, response)
                    try {
                        requestLock.lock()
                        apiRequests.remove(responseID)
                    } catch (ignored: Exception) {
                    } finally {
                        requestLock.unlock()
                    }
                }else {
                    val subscribing = subscriptions[matchedSubscription]
                    try {
                        matchedSubscription?.onCallback(response.get(JSON_MESSAGE_SUCCESS)?.asBoolean
                                ?: false)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (subscribing == false)
                        try {
                            subscriptionLock.lock()
                            subscriptions.remove(matchedSubscription)
                        } catch (ignored: Exception) {
                        } finally {
                            subscriptionLock.unlock()
                        }
                }

            }
            else ->
                if(matchedResponse != null) {
                    try {
                        matchedResponse.onMessage(this.webSocket, response)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    try {
                        subscriptionLock.lock()
                        apiRequests.remove(responseID)
                    } catch (ignored: Exception) {} finally {
                        subscriptionLock.unlock()
                    }
                }
        }
    }

    override fun onOpen(webSocket: WebSocket?, response: Response?) {
        connected = true
        handlePendingRequest()

        @Suppress("ConstantConditionIf")
        if (PING_INTERVAL > 0)
            pingTimer.schedule(object : TimerTask() {
                override fun run() {
                    if (!connected) {
                        cancel()
                        return
                    }

                    addRequest(Ping())
                }
            }, PING_INTERVAL, PING_INTERVAL)

    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String?) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null)
        resetState()
    }

    override fun onFailure(webSocket: WebSocket?, t: Throwable, response: Response?) {
        t.printStackTrace()
    }

    private fun sendRequest(request: WebsocketApiRequest<*>) {
        try {
            requestLock.lock()
            val requestID = nextID
            apiRequests[requestID] = request

            val generatedRequest = request.createRequest()
            generatedRequest.addProperty(JSON_MESSAGE_ID, requestID)
            webSocket.send(generatedRequest.toString())
        } catch (ignored: Exception) {} finally {
            requestLock.unlock()
        }
    }

    fun addRequest(request: WebsocketApiRequest<*>) {
        if(!queueNeeded) {
            sendRequest(request)
            return
        }

        try {
            pendingLock.lock()
            pendingRequests.add(request)

            if(session.expired)
                refreshToken()
        } catch (ignored: Exception) {} finally {
            pendingLock.unlock()
        }
    }

    fun subscribe(subscription: Subscription, eventType: String?) {
        if (subscription.id != -1)
            return

        if (queueNeeded) {
            try {
                subscriptionLock.lock()
                pendingSubscriptions[subscription] = SubscriptionStatus(true, eventType)

                if(session.expired)
                    refreshToken()
            } catch (ignored: Exception) {} finally {
                subscriptionLock.unlock()
            }

            return
        }

        subscriptions[subscription] = true
        webSocket.send(subscription.subscribeRequest(nextID, eventType).toString())
    }

    fun unsubscribe(subscription: Subscription) {
        if (subscription.id == -1)
            return

        if (queueNeeded) {
            try {
                subscriptionLock.lock()
                if (pendingSubscriptions.containsKey(subscription))
                    pendingSubscriptions.remove(subscription)
                else
                    pendingSubscriptions[subscription] = SubscriptionStatus(false, null)

                if(session.expired)
                    refreshToken()
            } catch (ignored: Exception) {} finally {
                subscriptionLock.unlock()
            }

            return
        }

        if (!subscriptions.containsKey(subscription))
            return

        subscriptions[subscription] = false

        val request = subscription.unsubscribeRequest()
        request.addProperty(JSON_MESSAGE_ID, nextID)
        webSocket.send(request.toString())
    }

    private fun handlePendingRequest() {
        if (queueNeeded) {
            if(session.expired)
                refreshToken()
            return
        }

        try {
            pendingLock.lock()
            for (pendingRequest in pendingRequests)
                sendRequest(pendingRequest)
            pendingRequests.clear()
        } catch (ignored: Exception) {} finally {
            pendingLock.unlock()
        }
    }

    private fun handlePendingSubscriptions() {
        if (queueNeeded) {
            if(session.expired)
                refreshToken()
            return
        }

        try {
            subscriptionLock.lock()
            for (pendingSubscriptionEntry in pendingSubscriptions) {
                val subscription = pendingSubscriptionEntry.key
                val pendingSubscriptionState = pendingSubscriptionEntry.value
                if(pendingSubscriptionState.subscribing)
                    subscribe(subscription, pendingSubscriptionState.eventType)
                else
                    unsubscribe(subscription)
            }

            pendingSubscriptions.clear()
        } catch (ignored: Exception) {} finally {
            subscriptionLock.unlock()
        }
    }

    private fun refreshToken() {
        interactionListener.onTokenRefreshNeeded(onTokenRefreshed)
    }

    private fun authenticate() {
        if (pendingAuthentication)
            return

        if(session.expired) {
            refreshToken()
            return
        }

        authenticated = false
        pendingAuthentication = true
        webSocket.send(Authenticate(session).createRequest().toString())
    }

    fun connect() {
        if (connected)
            return

        webSocket = client.newWebSocket(Request.Builder().url(websocketEndpoint).build(), this)
        client.dispatcher().executorService().shutdown()
    }

    fun disconnect() {
        if(!connected)
            return

        webSocket.close(NORMAL_CLOSURE_STATUS, "Bye!")
        resetState()
    }

    private fun resetState() {
        connected = false
        authenticated = false
        pendingAuthentication = false
        pingTimer.cancel()
        pendingRequests.clear()
        pendingSubscriptions.clear()
        subscriptions.clear()
        apiRequests.clear()
    }

    private data class SubscriptionStatus(val subscribing: Boolean, val eventType: String?)
}
