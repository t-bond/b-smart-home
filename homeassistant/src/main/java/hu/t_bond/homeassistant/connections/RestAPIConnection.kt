package hu.t_bond.homeassistant.connections

import hu.t_bond.homeassistant.listeners.ResponseListener
import hu.t_bond.homeassistant.model.Session
import hu.t_bond.homeassistant.requests.RefreshAccessToken
import hu.t_bond.homeassistant.requests.RestApiRequest
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.CountDownLatch


internal class RestAPIConnection(private val session: Session, private val baseURL: String) {

    private lateinit var restAPI: RestAPIService
    private var httpClient = OkHttpClient.Builder().addInterceptor(AuthorizationInterceptor()).build()

    init {
        connect()
    }

    fun connect() {
        restAPI = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .baseUrl(baseURL)
                .build().create(RestAPIService::class.java)
    }

    fun sendRequest(request: RestApiRequest<*>) {
        request.sendWith(restAPI)
    }

    private inner class AuthorizationInterceptor : Interceptor {

        override fun intercept(chain: Interceptor.Chain): Response {
            val specialType = chain.request().headers(HEADER_REQUEST_TYPE).firstOrNull()
            val isRefreshAccessToken = specialType == RefreshAccessToken.REFRESH_TOKEN_TAG
            val builder = chain.request().newBuilder()
            val startedToken = session.token
            builder.removeHeader(HEADER_REQUEST_TYPE)
            setupAuthorization(builder)

            var request = builder.build()
            val response: Response = chain.proceed(request) // Perform original request
            if(isRefreshAccessToken)
                return response

            if (response.code() == 401) { // On unauthorized
                synchronized(httpClient) {
                    val refreshRequest = RefreshAccessToken(session)

                    val loginLatch = CountDownLatch(1)
                    var gotTokenRefresh = false

                    refreshRequest.responseListener = object : ResponseListener<Boolean>() {
                        override fun onResult(result: Boolean) {
                            synchronized(httpClient) {
                                gotTokenRefresh = true
                                loginLatch.countDown()
                            }
                        }
                    }
                    sendRequest(refreshRequest)

                    // Wait for token update
                    while (!gotTokenRefresh)
                        loginLatch.await()

                    return if(session.token != startedToken) {
                        setupAuthorization(builder)
                        request = builder.build()

                        chain.proceed(request)
                    }else
                        response
                }
            }

            return response
        }

        private fun setupAuthorization(builder: Request.Builder) {
            builder.header("Authorization", "Bearer ${session.token}")
        }
    }

    companion object {
        const val HEADER_REQUEST_TYPE = "REQUEST_TYPE"
    }
}