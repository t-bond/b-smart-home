package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.ShoppingListItem
import okhttp3.WebSocket

class UpdateShoppingListItem(updatedItem: ShoppingListItem) : WebsocketApiRequest<Boolean>() {

    override val type: String = "shopping_list/items/update"

    private val updatedItem: ShoppingListItem = updatedItem.copy()

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        responseListener?.onResult(response.get("success").asBoolean)
    }

    override fun createRequest(): JsonObject {
        return super.createRequest().apply {
            addProperty("item_id", updatedItem.id)
            addProperty("name", updatedItem.name)
            addProperty("complete", updatedItem.complete)
        }
    }
}
