package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.services.Service
import okhttp3.WebSocket

class CallService(private val service: Service) : WebsocketApiRequest<Boolean>() {

    override val type: String = "call_service"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        responseListener?.onResult(response["success"]?.asBoolean ?: false)
    }

    override fun createRequest(): JsonObject {
        return super.createRequest().apply {
            addProperty("domain", service.domain)
            addProperty("service", service.type)
            service.getData().also { add("service_data", it) }
        }
    }
}
