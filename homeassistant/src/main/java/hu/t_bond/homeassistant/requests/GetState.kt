package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.HandledCallback
import retrofit2.Call
import retrofit2.Response

class GetState(private val entityID: String) : RestApiRequest<JsonObject>() {

    override fun sendWith(restAPI: RestAPIService) {
        restAPI.getState(entityID).enqueue(object : HandledCallback<JsonObject>(responseListener) {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val body = response.body()
                if(body != null)
                    responseListener?.onResult(body)
            }
        })
    }
}
