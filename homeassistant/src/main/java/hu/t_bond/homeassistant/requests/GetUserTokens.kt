package hu.t_bond.homeassistant.requests

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import hu.t_bond.homeassistant.model.Token
import okhttp3.WebSocket

class GetUserTokens : WebsocketApiRequest<List<Token>>() {

    override val type: String = "auth/refresh_tokens"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        val body = response.get("result")
        if(body == null) {
            responseListener?.onResult(emptyList())
            return
        }

        val type = TypeToken.getParameterized(List::class.java, Token::class.java).type
        responseListener?.onResult(Gson().fromJson(body, type))
    }
}
