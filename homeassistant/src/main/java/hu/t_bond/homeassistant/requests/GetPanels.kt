package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import okhttp3.WebSocket

class GetPanels : WebsocketApiRequest<JsonObject>() { /// TODO: Extract to a model

    override val type: String = "get_panels"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        if (response.has("success") && response.get("success").asBoolean)
            responseListener?.onResult(response.get("result").asJsonObject)
        else
            responseListener?.onResult(JsonObject())
    }
}
