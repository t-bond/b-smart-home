package hu.t_bond.homeassistant.requests

import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.ResponseListener

abstract class RestApiRequest<T> {
    var responseListener: ResponseListener<T>? = null

    internal abstract fun sendWith(restAPI: RestAPIService)
}
