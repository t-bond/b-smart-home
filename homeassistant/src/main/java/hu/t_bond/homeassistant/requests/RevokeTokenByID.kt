package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.Session
import okhttp3.WebSocket

class RevokeTokenByID(private val session: Session) : WebsocketApiRequest<Unit>() {

    override val type: String = "auth/delete_refresh_token"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        responseListener?.onResult(Unit)
    }


    override fun createRequest(): JsonObject {
        return super.createRequest().apply {
            addProperty("refresh_token_id", session.tokenID)
        }
    }
}
