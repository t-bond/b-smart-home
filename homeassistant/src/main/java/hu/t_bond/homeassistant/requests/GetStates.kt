package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.entities.Entity
import okhttp3.WebSocket

class GetStates : WebsocketApiRequest<List<Entity>>() {

    override val type: String = "get_states"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        responseListener?.also { responseListener ->
            if (response.get("success")?.asBoolean == true) {
                val entityList = mutableListOf<Entity>()
                for (jsonElement in response["result"].asJsonArray.toList())
                    try {
                        entityList.add(Entity.from(jsonElement.asJsonObject))
                    } catch (ignored: Exception) {
                    }

                responseListener.onResult(entityList.toList())
            } else
                responseListener.onResult(emptyList())
        }
    }
}
