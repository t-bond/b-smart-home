package hu.t_bond.homeassistant.requests

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.HandledCallback
import retrofit2.Call
import retrofit2.Response

class GetHistory(private val startTime: String?, private val filterEntityID: String?, private val endTime: String?) : RestApiRequest<List<String>>() {

    override fun sendWith(restAPI: RestAPIService) {
        restAPI.getHistory(startTime, filterEntityID, endTime).enqueue(object : HandledCallback<JsonArray>(responseListener) {
            override fun onResponse(call: Call<JsonArray>, response: Response<JsonArray>) {
                val body = response.body()
                if(body == null) {
                    responseListener?.onResult(emptyList())
                    return
                }

                val type = TypeToken.getParameterized(List::class.java, String::class.java).type
                responseListener?.onResult(Gson().fromJson(body, type))
            }
        })
    }
}
