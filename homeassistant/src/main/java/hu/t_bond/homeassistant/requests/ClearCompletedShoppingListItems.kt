package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import okhttp3.WebSocket

class ClearCompletedShoppingListItems : WebsocketApiRequest<Boolean>() {

    override val type: String = "shopping_list/items/clear"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        responseListener?.onResult(response.get("success")?.asBoolean ?: false)
    }

}
