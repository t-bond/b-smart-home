package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.Session

internal class Authenticate(private val session: Session) : WebsocketApiRequest<Unit>() {

    override val type: String = "auth"

    override fun createRequest(): JsonObject {
        return super.createRequest().apply {
            addProperty("access_token", session.token)
        }
    }
}
