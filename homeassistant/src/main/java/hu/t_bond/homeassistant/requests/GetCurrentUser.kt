package hu.t_bond.homeassistant.requests

import com.google.gson.Gson
import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.User
import okhttp3.WebSocket

class GetCurrentUser: WebsocketApiRequest<User?>() {

    override val type: String = "auth/current_user"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        responseListener?.onResult(
                if (response.get("success")?.asBoolean == true)
                    Gson().fromJson(response.get("result"), User::class.java)
                else null)
    }
}
