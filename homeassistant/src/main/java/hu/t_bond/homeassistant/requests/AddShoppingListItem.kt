package hu.t_bond.homeassistant.requests

import com.google.gson.Gson
import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.ShoppingListItem
import okhttp3.WebSocket

class AddShoppingListItem(private val name: String) : WebsocketApiRequest<ShoppingListItem>() {

    override val type: String = "shopping_list/items/add"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        if (response.get("success")?.asBoolean == true)
            responseListener?.onResult(Gson().fromJson(response["result"], ShoppingListItem::class.java))
    }

    override fun createRequest(): JsonObject {
        return super.createRequest().apply {
            addProperty("name", name)
        }
    }
}
