package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.HandledCallback
import retrofit2.Call
import retrofit2.Response

class UpdateAutomation(private val automationID: String, private val automationData: JsonObject) : RestApiRequest<Boolean>() {

    override fun sendWith(restAPI: RestAPIService) {
        restAPI.updateAutomation(automationID, automationData).enqueue(object : HandledCallback<JsonObject>(responseListener) {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (responseListener == null)
                    return

                val data = response.body()
                if (!response.isSuccessful || data == null || !data.has(JSON_RESULT)) {
                    responseListener?.onError(RuntimeException("Request does not succeeded."))
                    return
                }

                responseListener?.onResult(JSON_RESULT_OK == data.get(JSON_RESULT).asString)
            }
        })
    }

    companion object {
        private const val JSON_RESULT = "result"
        private const val JSON_RESULT_OK = "ok"
    }
}
