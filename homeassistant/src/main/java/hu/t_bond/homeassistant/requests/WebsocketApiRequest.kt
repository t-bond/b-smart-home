package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.connections.WebsocketConnection

import hu.t_bond.homeassistant.listeners.ResponseListener
import okhttp3.WebSocket

abstract class WebsocketApiRequest<T> {

    var responseListener: ResponseListener<T>? = null

    protected abstract val type: String

    internal open fun onMessage(webSocket: WebSocket, response: JsonObject) {}

    internal open fun createRequest(): JsonObject {
        return JsonObject().apply {
            addProperty(WebsocketConnection.JSON_MESSAGE_TYPE, type)
        }
    }
}
