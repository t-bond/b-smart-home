package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import okhttp3.WebSocket

class Ping : WebsocketApiRequest<Unit>() {

    override val type: String = "get_panels"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        responseListener?.onResult(Unit)
    }
}
