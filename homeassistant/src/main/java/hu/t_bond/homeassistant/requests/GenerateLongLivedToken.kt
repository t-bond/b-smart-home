package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.Session
import okhttp3.WebSocket
import java.text.SimpleDateFormat
import java.util.*

class GenerateLongLivedToken(private val clientID: String, private val clientName: String, private val clientIcon: String, private val requestExpiry: Int = DEFAULT_EXPIRY) : WebsocketApiRequest<Session?>() {

    override val type: String = "auth/long_lived_access_token"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        if (response.get("success")?.asBoolean == true)
            responseListener?.onResult(Session(clientID, response.get("result").asString, requestExpiry))
        else
            responseListener?.onResult(null)
    }


    override fun createRequest(): JsonObject {
        return super.createRequest().apply {
            addProperty("client_name", "$clientName (${SimpleDateFormat.getDateTimeInstance().format(Date())})")
            addProperty("client_icon", clientIcon)
            addProperty("lifespan", requestExpiry)
        }
    }

    companion object {

        const val DEFAULT_EXPIRY = 3650 // In days
    }
}
