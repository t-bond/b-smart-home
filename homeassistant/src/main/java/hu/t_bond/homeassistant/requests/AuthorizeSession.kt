package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.HandledCallback
import hu.t_bond.homeassistant.model.Session
import retrofit2.Call
import retrofit2.Response
import java.util.*

class AuthorizeSession(private val clientID: String, private val authorizationCode: String): RestApiRequest<Session?>() {

    override fun sendWith(restAPI: RestAPIService) {
        restAPI.authorizationCodeToRefreshToken(clientID, authorizationCode).enqueue(object : HandledCallback<JsonObject>(responseListener) {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if(responseListener == null) {
                    responseListener?.onResult(null)
                    return
                }

                val body = response.body()
                try {
                    val accessToken = body!!.get("access_token")!!.asString
                    val refreshToken = body.get("refresh_token")!!.asString
                    val expiresIn = body.get("expires_in")!!.asInt

                    val calendar = Calendar.getInstance()
                    calendar.add(Calendar.SECOND, expiresIn)

                    responseListener?.onResult(Session(clientID, accessToken, refreshToken, calendar.time))
                } catch (ignored: Exception) {
                    responseListener?.onResult(null)
                }
            }
        })
    }
}
