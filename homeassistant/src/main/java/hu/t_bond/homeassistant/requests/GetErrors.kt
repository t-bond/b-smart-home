package hu.t_bond.homeassistant.requests

import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.HandledCallback
import retrofit2.Call
import retrofit2.Response

class GetErrors: RestApiRequest<String?>() {

    override fun sendWith(restAPI: RestAPIService) {
        restAPI.errors.enqueue(object : HandledCallback<String>(responseListener) {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                responseListener?.onResult(response.body())
            }
        })
    }
}
