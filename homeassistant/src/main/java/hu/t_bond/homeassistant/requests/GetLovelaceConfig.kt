package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.LovelaceConfig
import hu.t_bond.homeassistant.model.View
import hu.t_bond.homeassistant.model.entities.Entities
import okhttp3.WebSocket

class GetLovelaceConfig(private val entityStore: Entities) : WebsocketApiRequest<LovelaceConfig>() {

    override val type: String = "lovelace/config"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        responseListener?.run {
            if (response[JSON_SUCCESS]?.asBoolean == true) {
                val result = response[JSON_RESULT]!!.asJsonObject!!

                val hasConfig = JSON_CODE_CONFIG_NOT_FOUND != result[JSON_RESULT_CODE]?.asString

                if (hasConfig)
                    onResult(LovelaceConfig(true, result[JSON_RESULT_VIEWS].asJsonArray.map { View.fromJson(entityStore, it.asJsonObject) }))
                else
                    onResult(LovelaceConfig(false, emptyList()))

            } else
                onError(RuntimeException("Could not get lovelace config: ${response.asString}"))


        }
    }


    companion object {
        private const val JSON_SUCCESS = "success"
        private const val JSON_RESULT = "result"
        private const val JSON_RESULT_CODE = "code"
        private const val JSON_RESULT_VIEWS = "views"
        private const val JSON_CODE_CONFIG_NOT_FOUND = "config_not_found"
    }
}
