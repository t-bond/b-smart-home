package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.HandledCallback
import hu.t_bond.homeassistant.model.Session
import retrofit2.Call
import retrofit2.Response
import java.util.*

class RefreshAccessToken(private val session: Session): RestApiRequest<Boolean>() {

    override fun sendWith(restAPI: RestAPIService) {
        val refreshToken = session.refreshToken
        if(session.longLived || !session.valid || refreshToken == null) {
            responseListener?.onResult(false)
            return
        }

        restAPI.renewAccessToken(session.clientID, refreshToken).enqueue(object : HandledCallback<JsonObject>(responseListener) {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    val body = response.body()
                    try {
                        val accessToken = body!!.get("access_token")!!.asString
                        val expiresIn = body.get("expires_in")!!.asInt

                        val calendar = Calendar.getInstance()
                        calendar.add(Calendar.MINUTE, expiresIn)

                        session.refresh(accessToken, calendar.time)
                        responseListener?.onResult(true)
                    } catch (ignored: Exception) {
                        responseListener?.onResult(false)
                    }
                }
            })
    }

    internal companion object {
        const val REFRESH_TOKEN_TAG = "RefreshAccessToken"
    }
}
