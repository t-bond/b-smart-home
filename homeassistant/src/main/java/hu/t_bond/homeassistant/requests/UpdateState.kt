package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.HandledCallback
import retrofit2.Call
import retrofit2.Response

class UpdateState(private val entityID: String, private val newState: JsonObject) : RestApiRequest<JsonObject>() { /// TODO: Extract to a model

    override fun sendWith(restAPI: RestAPIService) {
        restAPI.updateEntity(entityID, newState).enqueue(object : HandledCallback<JsonObject>(responseListener) {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                // response.code() == 200 on updated entity
                //                  == 201 on newly created status
                val body = response.body()
                if(body != null)
                    responseListener?.onResult(body)
            }
        })
    }
}
