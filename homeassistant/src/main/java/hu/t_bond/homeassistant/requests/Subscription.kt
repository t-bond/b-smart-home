package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.connections.WebsocketConnection
import hu.t_bond.homeassistant.listeners.ResponseListener
import java.util.*

class Subscription {
    var id = -1
        set(ID) {
            if (this.id == -1)
                field = ID
        }

    private val onResponseListeners = ArrayList<ResponseListener<JsonObject>>()
    private val onCallbackListeners = ArrayList<ResponseListener<Boolean>>()

    fun addResponseListener(responseListener: ResponseListener<JsonObject>) {
        onResponseListeners.add(responseListener)
    }

    fun addCallbackListener(callbackListeners: ResponseListener<Boolean>) {
        onCallbackListeners.add(callbackListeners)
    }

    fun removeResponseListener(responseListener: ResponseListener<JsonObject>) {
        onResponseListeners.remove(responseListener)
    }

    fun removeCallbackListener(callbackListeners: ResponseListener<Boolean>) {
        onCallbackListeners.remove(callbackListeners)
    }

    fun onResponse(response: JsonObject) {
        if (id == -1)
            return

        for (onResponseListener in onResponseListeners)
            onResponseListener.onResult(response)
    }

    fun onCallback(success: Boolean) {
        if (id == -1)
            return

        for (onCallbackListener in onCallbackListeners)
            onCallbackListener.onResult(success)
    }

    internal fun subscribeRequest(id: Int, eventType: String?): JsonObject {
        if (this.id != -1 && id != this.id)
            throw RuntimeException("Can not send the same subscription event twice")
        this.id = id

        val request = JsonObject()
        request.addProperty(WebsocketConnection.JSON_MESSAGE_TYPE, JSON_TYPE_SUBSCRIBE_EVENTS)
        request.addProperty(WebsocketConnection.JSON_MESSAGE_ID, id)
        if (eventType != null)
            request.addProperty(JSON_EVENT_TYPE, eventType)

        return request
    }

    internal fun unsubscribeRequest(): JsonObject {
        val request = JsonObject()
        request.addProperty(WebsocketConnection.JSON_MESSAGE_TYPE, JSON_TYPE_UNSUBSCRIBE_EVENTS)
        request.addProperty(JSON_MESSAGE_SUBSCRIPTION, id)

        return request
    }

    enum class EVENT(private val value: String) {
        STATE_CHANGED("state_changed");

        override fun toString() = value
    }

    companion object {
        const val JSON_MESSAGE_SUBSCRIPTION = "subscription"
        const val JSON_TYPE_UNSUBSCRIBE_EVENTS = "unsubscribe_events"
        const val JSON_TYPE_SUBSCRIBE_EVENTS = "subscribe_events"
        const val JSON_EVENT_TYPE = "event_type"
    }
}
