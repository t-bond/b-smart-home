package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.HandledCallback
import retrofit2.Call
import retrofit2.Response

class CheckConfig: RestApiRequest<Boolean>() {

    override fun sendWith(restAPI: RestAPIService) {
        restAPI.isConnected.enqueue(object : HandledCallback<JsonObject>(responseListener) {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                responseListener?.onResult("valid" == response.body()?.get("result")?.asString)
            }
        })
    }
}
