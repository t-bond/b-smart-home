package hu.t_bond.homeassistant.requests

import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.HandledCallback
import hu.t_bond.homeassistant.model.Session
import retrofit2.Call
import retrofit2.Response

class RevokeRefreshToken(private val session: Session): RestApiRequest<Unit>() {

    override fun sendWith(restAPI: RestAPIService) {
        if(session.refreshToken == null) {
            responseListener?.onResult(Unit)
            return
        }

        session.refreshToken?.let {
            restAPI.revokeRefreshToken(it).enqueue(object : HandledCallback<Unit>(responseListener) {
                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    session.revokeRefreshToken()
                    responseListener?.onResult(Unit)
                }
            })
        }
    }
}
