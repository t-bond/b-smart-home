package hu.t_bond.homeassistant.requests

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.connections.RestAPIService
import hu.t_bond.homeassistant.listeners.HandledCallback
import hu.t_bond.homeassistant.model.entities.AutomationEntity
import retrofit2.Call
import retrofit2.Response

class GetAutomation(private val automation: AutomationEntity) : RestApiRequest<Boolean>() {

    override fun sendWith(restAPI: RestAPIService) {
        if (!automation.isValid)
            return

        restAPI.getAutomation(automation.automationID!!).enqueue(object : HandledCallback<JsonObject>(responseListener) {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                response.body()?.also { config ->
                    automation.updateConfig(config)
                    responseListener?.onResult(true)
                    return
                }

                responseListener?.onResult(false)
            }
        })
    }
}
