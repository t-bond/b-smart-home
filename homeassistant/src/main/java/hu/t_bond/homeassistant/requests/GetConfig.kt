package hu.t_bond.homeassistant.requests

import com.google.gson.Gson
import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.Config
import okhttp3.WebSocket

class GetConfig: WebsocketApiRequest<Config?>() {

    override val type: String = "get_config"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        responseListener?.onResult(
                if (response.get("success")?.asBoolean == true)
                    Gson().fromJson(response.get("result"), Config::class.java)
                else null)
    }
}
