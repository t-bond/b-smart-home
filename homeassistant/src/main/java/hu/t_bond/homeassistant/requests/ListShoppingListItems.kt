package hu.t_bond.homeassistant.requests

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import hu.t_bond.homeassistant.model.ShoppingListItem
import okhttp3.WebSocket

class ListShoppingListItems : WebsocketApiRequest<List<ShoppingListItem>>() {

    override val type: String = "shopping_list/items"

    override fun onMessage(webSocket: WebSocket, response: JsonObject) {
        if (responseListener == null)
            return

        val items = response.get("result")?.asJsonArray
        if (response.get("success")?.asBoolean == true && items != null) {
            val type = TypeToken.getParameterized(List::class.java, ShoppingListItem::class.java).type
            responseListener?.onResult(Gson().fromJson(items, type))
        }else
            responseListener?.onResult(emptyList())
    }

}
