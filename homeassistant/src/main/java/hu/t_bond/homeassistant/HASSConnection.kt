package hu.t_bond.homeassistant

import hu.t_bond.homeassistant.connections.RestAPIConnection
import hu.t_bond.homeassistant.connections.WebsocketConnection
import hu.t_bond.homeassistant.listeners.InteractionListener
import hu.t_bond.homeassistant.listeners.ResponseListener
import hu.t_bond.homeassistant.model.Session
import hu.t_bond.homeassistant.model.entities.Entities
import hu.t_bond.homeassistant.requests.RefreshAccessToken
import hu.t_bond.homeassistant.requests.RestApiRequest
import hu.t_bond.homeassistant.requests.Subscription
import hu.t_bond.homeassistant.requests.WebsocketApiRequest
import java.io.Serializable

class HASSConnection(val session: Session, baseURL: String) : Serializable, InteractionListener {

    val baseURL = if (!baseURL.endsWith("/"))
                        "$baseURL/"
                    else
                        baseURL

    val entityStore = Entities(this)

    private val websocket = WebsocketConnection(session, baseURL, this)
    private val restAPI = RestAPIConnection(session, baseURL)

    fun addRequest(request: RestApiRequest<*>) {
        restAPI.sendRequest(request)
    }

    fun addRequest(request: WebsocketApiRequest<*>) {
        websocket.addRequest(request)
    }

    fun subscribe(subscription: Subscription, eventType: Subscription.EVENT) {
        websocket.subscribe(subscription, eventType.toString())
    }

    fun subscribe(subscription: Subscription, eventType: String?) {
        websocket.subscribe(subscription, eventType)
    }

    fun unsubscribe(subscription: Subscription) {
        websocket.unsubscribe(subscription)
    }

    fun reconnect() {
        websocket.disconnect()
        websocket.connect()
        restAPI.connect()
    }

    fun disconnect() {
        websocket.disconnect()
    }

    override fun onInvalidSession() {
    }

    override fun onTokenRefreshNeeded(passedResponseListener: ResponseListener<Boolean>) {
        RefreshAccessToken(session).apply {
            responseListener = passedResponseListener
            addRequest(this)
        }
    }

    companion object {
        const val SP_BASE_URL = "BASE_URL"

        @Suppress("SpellCheckingInspection")
        const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX"
    }
}
