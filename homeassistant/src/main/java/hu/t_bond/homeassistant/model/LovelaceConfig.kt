package hu.t_bond.homeassistant.model

data class LovelaceConfig(
        val hasConfig: Boolean,
        val views: List<View>
)
