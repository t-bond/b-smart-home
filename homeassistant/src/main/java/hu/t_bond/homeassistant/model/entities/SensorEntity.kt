package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject

class SensorEntity : Entity {
    override val entityType: ENTITY_TYPE = ENTITY_TYPE.SENSOR
    override val defaultIcon: String
        get() = "mdi:eye"

    var unitOfMeasurement: String? = null

    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from) {
        unitOfMeasurement = attributes[JSON_ATTRIBUTES_UNIT_OF_MEASUREMENT]
    }

    companion object {
        private const val JSON_ATTRIBUTES_UNIT_OF_MEASUREMENT = "unit_of_measurement"
    }
}
