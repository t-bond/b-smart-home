package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject

class DeviceTrackerEntity : Entity {
    override val entityType: ENTITY_TYPE = ENTITY_TYPE.DEVICE_TRACKER

    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from)
}