package hu.t_bond.homeassistant.model

data class Config (
        val components: List<Component>,
        val config_dir : String,
        val elevation : Int,
        val latitude : Double,
        val location_name : String,
        val longitude : Double,
        val time_zone : String,
        val unit_system : UnitSystem,
        val version : String,
        val whitelist_external_dirs : List<String>
)