package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject

class MediaPlayerEntity : Entity {
    override val entityType: ENTITY_TYPE = ENTITY_TYPE.MEDIA_PLAYER

    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from)
}