package hu.t_bond.homeassistant.model.cards

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.entities.Entities
import hu.t_bond.homeassistant.model.entities.Entity

class EntitiesCard(entitiesStore: Entities, from: JsonObject) : Card() {
    override val cardType = CARD_TYPE.ENTITIES

    var entities: List<Entity>
    var showHeaderToggle = from[JSON_SHOW_HEADER_TOGGLE]?.asBoolean ?: true
    var title = from[JSON_TITLE]?.asString

    init {
        val entityList = mutableListOf<Entity>()
        for (entityElement in from[JSON_ENTITIES].asJsonArray) {
            val entityID = if (entityElement.isJsonObject)
                entityElement.asJsonObject[JSON_ENTITIES_KEY]?.asString
            else
                entityElement.asString

            if (entityID != null)
                entityList.add(entitiesStore.getEntity(entityID))
            /// TODO: Handle non-entities
        }

        entities = entityList.toList()
    }

    companion object {
        private const val JSON_ENTITIES = "entities"
        private const val JSON_ENTITIES_KEY = "entity"
        private const val JSON_SHOW_HEADER_TOGGLE = "show_header_toggle"
        private const val JSON_TITLE = "title"
    }
}
