package hu.t_bond.homeassistant.model

data class User (
        val id : String,
        val name : String,
        val is_owner : Boolean,
        val credentials : List<Credentials>,
        val mfa_modules : List<Mfa_modules>
) {
    data class Credentials (
            val auth_provider_type : String,
            val auth_provider_id : String
    )

    data class Mfa_modules (
            val id : String,
            val name : String,
            val enabled : Boolean
    )
}