package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject
import java.util.*

enum class ENTITY_TYPE {
    AUTOMATION,
    BINARY_SENSOR,
    CLIMATE,
    DEVICE_TRACKER,
    INPUT_BOOLEAN,
    LIGHT,
    MEDIA_PLAYER,
    PERSON,
    SENSOR,
    SUN,
    SWITCH,
    WEATHER,
    ZONE,

    UNKNOWN;

    companion object {
        fun fromEntityID(entityID: String): ENTITY_TYPE {
            return try {
                valueOf(entityID.split(".", limit = 2)[0].toUpperCase(Locale.ROOT))
            } catch (ignored: Exception) {
                UNKNOWN
            }
        }

        fun fromJsonObject(jsonObject: JsonObject): ENTITY_TYPE {
            return fromEntityID(jsonObject[Entity.JSON_ENTITY_ID].asString)
        }
    }
}
