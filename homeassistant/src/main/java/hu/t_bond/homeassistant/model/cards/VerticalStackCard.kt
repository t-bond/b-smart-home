package hu.t_bond.homeassistant.model.cards

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.entities.Entities

class VerticalStackCard(entities: Entities, from: JsonObject) : Card() {
    override val cardType = CARD_TYPE.VERTICAL_STACK

    var cards: List<Card>
    var title = from[JSON_TITLE]?.asString

    init {
        val generatedCards = mutableListOf<Card>()
        from[JSON_CARDS]?.asJsonArray?.forEach {
            generatedCards.add(from(entities, it.asJsonObject))
        }

        cards = generatedCards.toList()
    }

    companion object {
        private const val JSON_CARDS = "cards"
        private const val JSON_TITLE = "title"
    }
}
