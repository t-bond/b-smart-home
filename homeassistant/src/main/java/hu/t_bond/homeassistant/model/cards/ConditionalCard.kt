package hu.t_bond.homeassistant.model.cards

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.entities.Entities
import hu.t_bond.homeassistant.model.entities.Entity

class ConditionalCard(entitiesStore: Entities, from: JsonObject) : Card() {
    override val cardType = CARD_TYPE.CONDITIONAL

    var conditions: List<Condition>
    var card: Card

    fun conditionsMet(): Boolean {
        for (condition in conditions)
            if (!condition.isConditionMet())
                return false
        return true
    }

    init {
        card = from(entitiesStore, from[JSON_CARD].asJsonObject)

        val conditionList = mutableListOf<Condition>()
        for (conditionElement in from[JSON_CONDITIONS].asJsonArray) {
            val jsonCondition = conditionElement.asJsonObject
            val entity = entitiesStore.getEntity(jsonCondition[JSON_ENTITIES_KEY]?.asString!!)
            val state = jsonCondition[JSON_STATE]?.asString
            val stateNot = jsonCondition[JSON_STATE_NOT]?.asString

            if (state != null)
                conditionList.add(Condition(entity, state))
            else
                conditionList.add(Condition(entity, stateNot = stateNot))
        }

        conditions = conditionList.toList()
    }

    data class Condition(val entity: Entity, val state: String? = null, val stateNot: String? = null) {
        init {
            if (state == null && stateNot == null)
                throw IllegalArgumentException("One of the `state` or `state_not` is required.")
        }

        fun isConditionMet() =
                if (stateNot == null)
                    entity.state == state
                else
                    entity.state != stateNot
    }

    companion object {
        private const val JSON_CONDITIONS = "conditions"
        private const val JSON_ENTITIES_KEY = "entity"
        private const val JSON_STATE = "state"
        private const val JSON_STATE_NOT = "state_not"
        private const val JSON_CARD = "card"
    }
}
