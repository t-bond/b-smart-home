package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.HASSConnection
import java.text.SimpleDateFormat
import java.util.*

class AutomationEntity : Entity {

    override val entityType: ENTITY_TYPE = ENTITY_TYPE.AUTOMATION

    var automationID: String? = null
        private set
    var lastTriggered: Date = Date()
        private set
    var config: JsonObject = JsonObject() /// TODO: Implement automation config
        private set


    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from) {
        _initEntity(from)
    }

    override fun _initEntity(json: JsonObject) {
        if (isValid)
            return

        super._initEntity(json)

        automationID = attributes[JSON_ATTRIBUTES_ID]!!

        val inFormat = SimpleDateFormat(HASSConnection.DATE_FORMAT)
        lastTriggered = inFormat.parse(attributes[JSON_ATTRIBUTES_LAST_TRIGGERED]!!)!!

        attributes.remove(JSON_ATTRIBUTES_ID)
        attributes.remove(JSON_ATTRIBUTES_LAST_TRIGGERED)
    }

    fun updateConfig(config: JsonObject) {
        TODO("not implemented")
    }

    companion object {
        const val JSON_ATTRIBUTES_ID = "id"
        const val JSON_ATTRIBUTES_LAST_TRIGGERED = "last_triggered"
    }
}
