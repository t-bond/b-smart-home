package hu.t_bond.homeassistant.model

import com.google.gson.annotations.SerializedName

@Suppress("unused")
enum class Component {
    @SerializedName("frontend")
    FRONTEND,

    @SerializedName("shopping_list")
    SHOPPING_LIST,

    @SerializedName("device_automation")
    DEVICE_AUTOMATION,

    @SerializedName("climate.generic_thermostat")
    CLIMATE_GENERIC_THERMOSTAT,

    @SerializedName("binary_sensor")
    BINARY_SENSOR,

    @SerializedName("lovelace")
    LOVELACE,

    @SerializedName("http")
    HTTP,

    @SerializedName("logbook")
    LOGBOOK,

    @SerializedName("config")
    CONFIG,

    @SerializedName("group")
    GROUP,

    @SerializedName("switch.mqtt")
    SWITCH_MQTT,

    @SerializedName("light")
    LIGHT,

    @SerializedName("system_health")
    SYSTEM_HEALTH,

    @SerializedName("binary_sensor.mqtt")
    BINARY_SENSOR_MQTT,

    @SerializedName("input_boolean")
    INPUT_BOOLEAN,

    @SerializedName("sensor.mobile_app")
    SENSOR_MOBILE_APP,

    @SerializedName("media_player")
    MEDIA_PLAYER,

    @SerializedName("homeassistant")
    HOMEASSISTANT,

    @SerializedName("recorder")
    RECORDER,

    @SerializedName("sensor.mqtt")
    SENSOR_MQTT,

    @SerializedName("weather")
    WEATHER,

    @SerializedName("script")
    SCRIPT,

    @SerializedName("updater")
    UPDATER,

    @SerializedName("system_log")
    SYSTEM_LOG,

    @SerializedName("sun")
    SUN,

    @SerializedName("search")
    SEARCH,

    @SerializedName("mqtt")
    MQTT,

    @SerializedName("device_tracker")
    DEVICE_TRACKER,

    @SerializedName("mobile_app")
    MOBILE_APP,

    @SerializedName("sensor")
    SENSOR,

    @SerializedName("switch")
    SWITCH,

    @SerializedName("auth")
    AUTH,

    @SerializedName("zone")
    ZONE,

    @SerializedName("device_tracker.mobile_app")
    DEVICE_TRACKER_MOBILE_APP,

    @SerializedName("onboarding")
    ONBOARDING,

    @SerializedName("map")
    MAP,

    @SerializedName("automation")
    AUTOMATION,

    @SerializedName("history")
    HISTORY,

    @SerializedName("webhook")
    WEBHOOK,

    @SerializedName("climate")
    CLIMATE,

    @SerializedName("light.mqtt")
    LIGHT_MQTT,

    @SerializedName("binary_sensor.updater")
    BINARY_SENSOR_UPDATER,

    @SerializedName("websocket_api")
    WEBSOCKET_API,

    @SerializedName("notify")
    NOTIFY,

    @SerializedName("binary_sensor.mobile_app")
    BINARY_SENSOR_MOBILE_APP,

    @SerializedName("person")
    PERSON,

    @SerializedName("api")
    API,

    @SerializedName("sensor.template")
    SENSOR_TEMPLATE
}