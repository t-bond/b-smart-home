package hu.t_bond.homeassistant.model

import java.io.Serializable
import java.util.*


class Session(clientID: String, token: String, private var expiry: Date) : Serializable {

    var clientID = clientID
        private set

    internal var tokenID: String? = null
        private set

    internal var token = token
        private set

    internal var refreshToken: String? = null
        private set

    var longLived = true
        private set

    val valid get() = !(refreshToken == null && expired)

    val expired get() = expiry.before(Date())

    constructor(clientID: String, token: String, refreshToken: String, expiryInDays: Int) : this(clientID, token, Date()) {
        this.refreshToken = refreshToken
        longLived = false
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, expiryInDays)
        expiry = calendar.time
    }

    constructor(clientID: String, token: String, expiryInDays: Int) : this(clientID, token, Date()) {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, expiryInDays)
        expiry = calendar.time
    }


    constructor() : this("", "", Date()) { // Invalid session creation
        longLived = false
    }

    constructor(clientID: String, token: String, refreshToken: String, expiry: Date) : this(clientID, token, expiry) {
        this.refreshToken = refreshToken
        longLived = false
    }

    internal fun refresh(accessToken: String, newExpiry: Date) {
        if(newExpiry.before(expiry))
            return

        token = accessToken
        expiry = newExpiry
    }

    fun revokeRefreshToken() {
        refreshToken = null
        expiry.time = 0

        if(!longLived)
            token = "INVALID"
    }
}
