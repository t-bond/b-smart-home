package hu.t_bond.homeassistant.model.cards

import com.google.gson.JsonObject
import java.util.*

enum class CARD_TYPE {
    ALARM_PANEL,
    BUTTON,
    GLANCE,
    IFRAME,
    MARKDOWN,
    PICTURE_ELEMENTS,
    PLANT_STATUS,
    THERMOSTAT,
    CONDITIONAL,
    ENTITY_FILTER,
    HISTORY_GRAPH,
    LIGHT,
    MEDIA_CONTROL,
    PICTURE_ENTITY,
    SENSOR,
    VERTICAL_STACK,
    ENTITIES,
    GAUGE,
    HORIZONTAL_STACK,
    MAP,
    PICTURE,
    PICTURE_GLANCE,
    SHOPPING_LIST,
    WEATHER_FORECAST,

    UNKNOWN;

    companion object {
        fun fromType(type: String): CARD_TYPE {
            return try {
                valueOf(type.toUpperCase(Locale.ROOT).replace('-', '_'))
            } catch (ignored: Exception) {
                UNKNOWN
            }
        }

        fun fromJsonObject(jsonObject: JsonObject): CARD_TYPE {
            return fromType(jsonObject[Card.JSON_TYPE].asString)
        }
    }
}
