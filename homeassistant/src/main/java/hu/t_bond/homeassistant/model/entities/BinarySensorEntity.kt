package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject

class BinarySensorEntity : Entity {
    override val entityType: ENTITY_TYPE = ENTITY_TYPE.BINARY_SENSOR

    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from)
}