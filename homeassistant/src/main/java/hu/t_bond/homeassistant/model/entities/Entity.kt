package hu.t_bond.homeassistant.model.entities

import com.google.gson.Gson
import com.google.gson.JsonObject
import hu.t_bond.homeassistant.HASSConnection
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


abstract class Entity(val entityID: String) : Serializable {

    abstract val entityType: ENTITY_TYPE
    open val defaultIcon = "mdi:alert"

    var isValid: Boolean = false
        private set

    var state = ""
    var friendlyName = ""
    var lastChanged: Date = Date()
        private set
    var lastUpdated: Date = lastChanged
        private set
    var icon: String? = null
        get() = field ?: defaultIcon

    var entityPicture: String? = null
    val attributes: HashMap<String, String?> = HashMap()
    var context: EntityContext? = null
        private set

    constructor(jsonObject: JsonObject) : this(jsonObject[JSON_ENTITY_ID].asString) {
        initEntity(jsonObject)
    }

    fun initEntity(json: JsonObject) {
        if (isValid)
            return

        if (json[JSON_ENTITY_ID].asString != entityID)
            throw IllegalArgumentException("Can't initialize entity with different entity id!")

        _initEntity(json)
    }

    protected open fun _initEntity(json: JsonObject) {
        if (isValid)
            return

        json.apply {
            state = this[JSON_STATE].asString
            context = Gson().fromJson(this[JSON_CONTEXT], EntityContext::class.java)

            val attr = get(JSON_ATTRIBUTES)!!.asJsonObject

            val inFormat = SimpleDateFormat(HASSConnection.DATE_FORMAT)

            friendlyName = attr[JSON_ATTRIBUTES_FRIENDLY_NAME].asString
            icon = attr[JSON_ATTRIBUTES_ICON]?.asString
            entityPicture = attr[JSON_ATTRIBUTES_ENTITY_PICTURE]?.asString
            lastChanged = inFormat.parse(get(JSON_LAST_CHANGED).asString)!!
            lastUpdated = inFormat.parse(get(JSON_LAST_UPDATED).asString)!!

            attr.remove(JSON_ATTRIBUTES_FRIENDLY_NAME)
            for (attributeEntry in attr.entrySet())
                attributes[attributeEntry.key] = attributeEntry.value.asString
        }

        isValid = true
    }

    internal open fun updateState(entity: Entity) {
        if (entityID != entity.entityID)
            throw IllegalArgumentException("Can't merge entities from different domains.")

        ///TODO: Make entity merge
    }

    internal companion object {
        fun from(json: JsonObject): Entity {
            return when (ENTITY_TYPE.fromJsonObject(json)) {
                ENTITY_TYPE.AUTOMATION -> AutomationEntity(json)
                ENTITY_TYPE.BINARY_SENSOR -> BinarySensorEntity(json)
                ENTITY_TYPE.CLIMATE -> ClimateEntity(json)
                ENTITY_TYPE.DEVICE_TRACKER -> DeviceTrackerEntity(json)
                ENTITY_TYPE.INPUT_BOOLEAN -> InputBooleanEntity(json)
                ENTITY_TYPE.LIGHT -> LightEntity(json)
                ENTITY_TYPE.MEDIA_PLAYER -> MediaPlayerEntity(json)
                ENTITY_TYPE.PERSON -> PersonEntity(json)
                ENTITY_TYPE.SENSOR -> SensorEntity(json)
                ENTITY_TYPE.SUN -> SunEntity(json)
                ENTITY_TYPE.SWITCH -> SwitchEntity(json)
                ENTITY_TYPE.WEATHER -> WeatherEntity(json)
                ENTITY_TYPE.ZONE -> ZoneEntity(json)
                ENTITY_TYPE.UNKNOWN -> UnknownEntity(json)
            }
        }

        fun from(entityID: String): Entity {
            return when (ENTITY_TYPE.fromEntityID(entityID)) {
                ENTITY_TYPE.AUTOMATION -> AutomationEntity(entityID)
                ENTITY_TYPE.BINARY_SENSOR -> BinarySensorEntity(entityID)
                ENTITY_TYPE.CLIMATE -> ClimateEntity(entityID)
                ENTITY_TYPE.DEVICE_TRACKER -> DeviceTrackerEntity(entityID)
                ENTITY_TYPE.INPUT_BOOLEAN -> InputBooleanEntity(entityID)
                ENTITY_TYPE.LIGHT -> LightEntity(entityID)
                ENTITY_TYPE.MEDIA_PLAYER -> MediaPlayerEntity(entityID)
                ENTITY_TYPE.PERSON -> PersonEntity(entityID)
                ENTITY_TYPE.SENSOR -> SensorEntity(entityID)
                ENTITY_TYPE.SUN -> SunEntity(entityID)
                ENTITY_TYPE.SWITCH -> SwitchEntity(entityID)
                ENTITY_TYPE.WEATHER -> WeatherEntity(entityID)
                ENTITY_TYPE.ZONE -> ZoneEntity(entityID)
                ENTITY_TYPE.UNKNOWN -> UnknownEntity(entityID)
            }
        }

        fun getDomain(entityID: String) = entityID.substringBefore(".")

        const val JSON_ATTRIBUTES = "attributes"
        const val JSON_ATTRIBUTES_FRIENDLY_NAME = "friendly_name"
        const val JSON_ATTRIBUTES_ICON = "icon"
        const val JSON_ATTRIBUTES_ENTITY_PICTURE = "icon"
        const val JSON_LAST_CHANGED = "last_changed"
        const val JSON_LAST_UPDATED = "last_updated"
        const val JSON_CONTEXT = "context"
        const val JSON_STATE = "state"
        const val JSON_ENTITY_ID = "entity_id"
    }
}
