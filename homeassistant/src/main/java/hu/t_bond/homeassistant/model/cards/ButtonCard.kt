package hu.t_bond.homeassistant.model.cards

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.entities.Entities
import hu.t_bond.homeassistant.model.entities.Entity

class ButtonCard(entities: Entities, from: JsonObject) : Card() {
    override val cardType = CARD_TYPE.BUTTON

    var entity: Entity = entities.getEntity(from[JSON_ENTITY].asString)
    var showName = from[JSON_SHOW_NAME].asBoolean
    var showIcon = from[JSON_SHOW_ICON].asBoolean
    var name = from[JSON_NAME]?.asString
    var icon = from[JSON_ICON]?.asString
    var iconHeight = from[JSON_ICON_HEIGHT]?.asString?.removeSuffix("px")?.toInt()
            ?: 200 /* in PX */ /// TODO: Get a default icon size

    companion object {
        private const val JSON_ENTITY = "entity"
        private const val JSON_SHOW_NAME = "show_name"
        private const val JSON_SHOW_ICON = "show_icon"
        private const val JSON_NAME = "name"
        private const val JSON_ICON = "icon"
        private const val JSON_ICON_HEIGHT = "icon_height"
    }
}
