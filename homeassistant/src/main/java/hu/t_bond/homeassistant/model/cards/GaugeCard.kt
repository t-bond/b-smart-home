package hu.t_bond.homeassistant.model.cards

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.entities.Entities
import hu.t_bond.homeassistant.model.entities.Entity

class GaugeCard(entities: Entities, from: JsonObject) : Card() {
    override val cardType = CARD_TYPE.GAUGE

    val entity: Entity = entities.getEntity(from[JSON_ENTITY].asString)
    val name = from[JSON_NAME]?.asString
    val unit = from[JSON_UNIT]?.asString
    val min = from[JSON_MIN]?.asInt ?: 0
    val max = from[JSON_MAX]?.asInt ?: 100

    var severityGreen: Int? = null
    var severityYellow: Int? = null
    var severityRed: Int? = null

    init {
        from[JSON_SEVERITY]?.asJsonObject?.also {
            severityGreen = it[JSON_SEVERITY_GREEN].asInt
            severityYellow = it[JSON_SEVERITY_YELLOW].asInt
            severityRed = it[JSON_SEVERITY_RED].asInt
        }
    }

    companion object {
        private const val JSON_ENTITY = "entity"
        private const val JSON_NAME = "name"
        private const val JSON_UNIT = "unit"
        private const val JSON_MIN = "min"
        private const val JSON_MAX = "max"
        private const val JSON_SEVERITY = "severity"
        private const val JSON_SEVERITY_GREEN = "green"
        private const val JSON_SEVERITY_YELLOW = "yellow"
        private const val JSON_SEVERITY_RED = "red"
    }
}
