package hu.t_bond.homeassistant.model

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.cards.Card
import hu.t_bond.homeassistant.model.entities.Entities
import hu.t_bond.homeassistant.model.entities.Entity
import java.io.Serializable

data class View(
        val badges: List<Entity>,
        val path: String,
        val title: String,
        val icon: String,
        val cards: List<Card>
) : Serializable {

    companion object {
        private const val JSON_BADGES = "badges"
        private const val JSON_BADGE_KEY = "entity"
        private const val JSON_PATH = "path"
        private const val JSON_TITLE = "title"
        private const val JSON_ICON = "icon"
        private const val JSON_CARDS = "cards"

        fun fromJson(entities: Entities, json: JsonObject): View {
            return View(
                    json[JSON_BADGES].asJsonArray.map { entities.getEntity(it.asJsonObject[JSON_BADGE_KEY].asString) },
                    json[JSON_PATH].asString,
                    json[JSON_TITLE].asString,
                    json[JSON_ICON].asString,
                    json[JSON_CARDS].asJsonArray.map { Card.from(entities, it.asJsonObject) }
            )
        }
    }
}
