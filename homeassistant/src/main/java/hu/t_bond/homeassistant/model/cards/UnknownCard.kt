package hu.t_bond.homeassistant.model.cards

class UnknownCard : Card() {
    override val cardType = CARD_TYPE.UNKNOWN
}
