package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject

class WeatherEntity : Entity {
    override val entityType: ENTITY_TYPE = ENTITY_TYPE.WEATHER

    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from)
}