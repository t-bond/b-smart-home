package hu.t_bond.homeassistant.model.cards

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.entities.Entities
import java.io.Serializable

abstract class Card : Serializable {
    abstract val cardType: CARD_TYPE

    companion object {
        internal const val JSON_TYPE = "type"

        fun from(entities: Entities, from: JsonObject): Card {
            return when (CARD_TYPE.fromJsonObject(from)) {
                CARD_TYPE.ENTITIES -> EntitiesCard(entities, from)
                CARD_TYPE.BUTTON -> ButtonCard(entities, from)
                CARD_TYPE.VERTICAL_STACK -> VerticalStackCard(entities, from)
                CARD_TYPE.HORIZONTAL_STACK -> HorizontalStackCard(entities, from)
                CARD_TYPE.CONDITIONAL -> ConditionalCard(entities, from)
                CARD_TYPE.LIGHT -> LightCard(entities, from)
                CARD_TYPE.GAUGE -> GaugeCard(entities, from)
                else -> UnknownCard()
            }
        }
    }
}
