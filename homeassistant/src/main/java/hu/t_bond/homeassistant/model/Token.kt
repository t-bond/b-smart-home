package hu.t_bond.homeassistant.model

data class Token (
        val id : String,
        val client_id : String,
        val client_name : String,
        val client_icon : String,
        val type : String,
        val created_at : String,
        val is_current : Boolean,
        val last_used_at : String,
        val last_used_ip : String
)