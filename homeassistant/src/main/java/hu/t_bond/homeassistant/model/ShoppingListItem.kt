package hu.t_bond.homeassistant.model

data class ShoppingListItem(val id: String, var name: String, var complete: Boolean)
