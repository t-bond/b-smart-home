package hu.t_bond.homeassistant.model

data class UnitSystem (
        val length : String,
        val mass : String,
        val temperature : String,
        val volume : String
)