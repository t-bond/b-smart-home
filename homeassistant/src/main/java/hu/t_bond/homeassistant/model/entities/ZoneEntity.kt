package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject

class ZoneEntity : Entity {
    override val entityType: ENTITY_TYPE = ENTITY_TYPE.ZONE

    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from)
}