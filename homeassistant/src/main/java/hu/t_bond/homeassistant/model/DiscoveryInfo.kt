package hu.t_bond.homeassistant.model

data class DiscoveryInfo (
        val base_url : String,
        val location_name : String,
        val requires_api_password : Boolean,
        val version : String
)