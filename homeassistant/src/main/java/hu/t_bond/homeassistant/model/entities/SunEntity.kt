package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.HASSConnection
import java.text.SimpleDateFormat
import java.util.*

class SunEntity : Entity {

    override val entityType: ENTITY_TYPE = ENTITY_TYPE.SUN

    var nextDawn: Date = Date()
        private set
    var nextDusk: Date = Date()
        private set
    var nextMidnight: Date = Date()
        private set
    var nextNoon: Date = Date()
        private set
    var nextRising: Date = Date()
        private set
    var nextSetting: Date = Date()
        private set
    var elevation: Double = 0.0
        private set
    var azimuth: Double = 0.0
        private set
    var rising: Boolean = false
        private set

    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from)

    override fun _initEntity(json: JsonObject) {
        if (isValid)
            return

        super._initEntity(json)

        val inFormat = SimpleDateFormat(HASSConnection.DATE_FORMAT)

        try {
            nextDawn = inFormat.parse(attributes[JSON_ATTRIBUTES_NEXT_DAWN]!!)!!
            nextDusk = inFormat.parse(attributes[JSON_ATTRIBUTES_NEXT_DUSK]!!)!!
            nextMidnight = inFormat.parse(attributes[JSON_ATTRIBUTES_NEXT_MIDNIGHT]!!)!!
            nextNoon = inFormat.parse(attributes[JSON_ATTRIBUTES_NEXT_NOON]!!)!!
            nextRising = inFormat.parse(attributes[JSON_ATTRIBUTES_NEXT_RISING]!!)!!
            nextSetting = inFormat.parse(attributes[JSON_ATTRIBUTES_NEXT_SETTING]!!)!!
            nextSetting = inFormat.parse(attributes[JSON_ATTRIBUTES_NEXT_SETTING]!!)!!
        } catch (ignored: Exception) {
        }

        elevation = attributes[JSON_ATTRIBUTES_ELEVATION]!!.toDouble()
        azimuth = attributes[JSON_ATTRIBUTES_AZIMUTH]!!.toDouble()

        rising = attributes[JSON_ATTRIBUTES_RISING]!!.toBoolean()

        attributes.remove(JSON_ATTRIBUTES_NEXT_DAWN)
        attributes.remove(JSON_ATTRIBUTES_NEXT_DUSK)
        attributes.remove(JSON_ATTRIBUTES_NEXT_MIDNIGHT)
        attributes.remove(JSON_ATTRIBUTES_NEXT_NOON)
        attributes.remove(JSON_ATTRIBUTES_NEXT_RISING)
        attributes.remove(JSON_ATTRIBUTES_NEXT_SETTING)
        attributes.remove(JSON_ATTRIBUTES_ELEVATION)
        attributes.remove(JSON_ATTRIBUTES_AZIMUTH)
        attributes.remove(JSON_ATTRIBUTES_RISING)
    }

    companion object {
        const val JSON_ATTRIBUTES_NEXT_DAWN = "next_dawn"
        const val JSON_ATTRIBUTES_NEXT_DUSK = "next_dusk"
        const val JSON_ATTRIBUTES_NEXT_MIDNIGHT = "next_midnight"
        const val JSON_ATTRIBUTES_NEXT_NOON = "next_noon"
        const val JSON_ATTRIBUTES_NEXT_RISING = "next_rising"
        const val JSON_ATTRIBUTES_NEXT_SETTING = "next_setting"
        const val JSON_ATTRIBUTES_ELEVATION = "elevation"
        const val JSON_ATTRIBUTES_AZIMUTH = "azimuth"
        const val JSON_ATTRIBUTES_RISING = "rising"
    }
}
