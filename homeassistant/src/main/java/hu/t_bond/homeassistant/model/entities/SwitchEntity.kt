package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject

class SwitchEntity : Entity {
    override val entityType: ENTITY_TYPE = ENTITY_TYPE.SWITCH
    override val defaultIcon = "mdi:flash"

    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from)
}
