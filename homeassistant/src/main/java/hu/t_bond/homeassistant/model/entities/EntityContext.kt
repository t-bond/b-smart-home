package hu.t_bond.homeassistant.model.entities

import java.io.Serializable

data class EntityContext(private val id: String, private val parent_id: String? = null, private val user_id: String? = null) : Serializable
