package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject

class LightEntity : Entity {
    override val entityType: ENTITY_TYPE = ENTITY_TYPE.LIGHT
    override val defaultIcon = "mdi:lightbulb"

    var brightness = 0

    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from)

    override fun _initEntity(json: JsonObject) {
        if (isValid)
            return

        super._initEntity(json)

        brightness = attributes[JSON_ATTRIBUTES_BRIGHTNESS]?.toInt() ?: 0
    }

    companion object {
        private const val JSON_ATTRIBUTES_BRIGHTNESS = "brightness"
    }
}
