package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject

class InputBooleanEntity : Entity {
    override val entityType: ENTITY_TYPE = ENTITY_TYPE.INPUT_BOOLEAN

    constructor(entityID: String) : super(entityID)
    constructor(from: JsonObject) : super(from)
}