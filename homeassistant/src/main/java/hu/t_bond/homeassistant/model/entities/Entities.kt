package hu.t_bond.homeassistant.model.entities

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.HASSConnection
import hu.t_bond.homeassistant.listeners.ResponseListener
import hu.t_bond.homeassistant.requests.GetStates
import hu.t_bond.homeassistant.requests.Subscription
import java.io.Serializable

class Entities(private val hassConnection: HASSConnection) : Serializable {
    var subscription: Subscription = Subscription()
    var automaticUpdates = false
        set(value) {
            if (field == value)
                return
            field = value

            if (value) {
                subscription = Subscription().apply {
                    addResponseListener(onEventData)
                }
                hassConnection.subscribe(subscription, Subscription.EVENT.STATE_CHANGED)

            } else
                hassConnection.unsubscribe(subscription)
        }

    private val entities = HashMap<String, Entity>()

    private val onEventData = object : ResponseListener<JsonObject>() {
        override fun onResult(result: JsonObject) {
            val data = result[JSON_EVENT_DATA]?.asJsonObject ?: return
            onStateData.onResult(listOf(Entity.from(data[JSON_EVENT_DATA_NEW_STATE].asJsonObject)))
        }
    }

    private val onStateData = object : ResponseListener<List<Entity>>() {
        override fun onResult(result: List<Entity>) {
            for (entity in result)
                if (entities[entity.entityID] != null) {
                    entities[entity.entityID]?.updateState(entity)
                } else
                    entities[entity.entityID] = entity
        }
    }

    fun update() {
        hassConnection.addRequest(GetStates().apply {
            responseListener = onStateData
        })
    }

    fun getEntities(filter: ENTITY_TYPE? = null): List<Entity> {
        val entityList = entities.values.toList()
        return if(filter == null)
            entityList
        else
            entityList.filter { it.entityType == filter }
    }

    fun getEntity(entityID: String): Entity {
        val matchedEntity = entities[entityID]
        if (matchedEntity == null) {
            val generatedEntity = Entity.from(entityID)
            entities[entityID] = generatedEntity
            return generatedEntity
        }

        return matchedEntity
    }

    companion object {
        private const val JSON_EVENT_DATA = "data"
        private const val JSON_EVENT_DATA_NEW_STATE = "new_state"
    }
}
