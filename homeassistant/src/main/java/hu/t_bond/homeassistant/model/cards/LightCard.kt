package hu.t_bond.homeassistant.model.cards

import com.google.gson.JsonObject
import hu.t_bond.homeassistant.model.entities.Entities
import hu.t_bond.homeassistant.model.entities.LightEntity

class LightCard(entities: Entities, from: JsonObject) : Card() {
    override val cardType = CARD_TYPE.LIGHT

    var entity: LightEntity = entities.getEntity(from[JSON_ENTITY].asString) as LightEntity
    var name = from[JSON_NAME]?.asString
    var icon = from[JSON_ICON]?.asString

    companion object {
        private const val JSON_ENTITY = "entity"
        private const val JSON_NAME = "name"
        private const val JSON_ICON = "icon"
    }
}
