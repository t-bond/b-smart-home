package hu.t_bond.homeassistant.listeners

import hu.t_bond.homeassistant.model.entities.Entity

interface EntityChange {
    fun onEntityAdded(entity: Entity)
}
