package hu.t_bond.homeassistant.listeners

import retrofit2.Call
import retrofit2.Callback

abstract class HandledCallback<T>(private val responseListener: ResponseListener<*>?) : Callback<T> {

    override fun onFailure(call: Call<T>, t: Throwable) {
        responseListener?.onError(t)
    }
}
