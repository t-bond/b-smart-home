package hu.t_bond.homeassistant.listeners

interface StateChangeListener {
    fun onVisibleStateChange()

    fun onStateChange()
}
