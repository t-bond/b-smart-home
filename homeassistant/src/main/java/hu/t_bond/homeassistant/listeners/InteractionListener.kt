package hu.t_bond.homeassistant.listeners

internal interface InteractionListener {
    fun onInvalidSession()
    fun onTokenRefreshNeeded(passedResponseListener: ResponseListener<Boolean>)
}