package hu.t_bond.homeassistant.listeners

abstract class ResponseListener<T> {
    abstract fun onResult(result: T)

    fun onError(t: Throwable) {
        t.printStackTrace()
    }
}
