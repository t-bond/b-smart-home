package hu.t_bond.b_smarthome.ui

import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import hu.t_bond.b_smarthome.R
import hu.t_bond.b_smarthome.databinding.FragmentShoppingListBinding
import hu.t_bond.b_smarthome.model.adapters.ShoppingListAdapter
import hu.t_bond.b_smarthome.model.viewmodel.ShoppingListViewModel
import kotlinx.android.synthetic.main.fragment_shopping_list.*

class ShoppingList : Fragment() {

    private lateinit var binding: FragmentShoppingListBinding
    private val viewModel: ShoppingListViewModel by viewModels()
    private lateinit var adapter: ShoppingListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentShoppingListBinding.inflate(inflater)
        binding.shoppingList = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = ShoppingListAdapter(viewModel.getItems())
        shoppingListItems.adapter = adapter

        viewModel.getItems().observe(viewLifecycleOwner, Observer {
            adapter.notifyDataSetChanged()
        })

        binding.newItemName.setOnEditorActionListener { _: TextView, actionID: Int, _: KeyEvent? ->
            if (actionID == EditorInfo.IME_ACTION_DONE) {
                addItem()
                return@setOnEditorActionListener true
            }
            false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.shopping_list_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.shopping_list_clear_completed -> viewModel.clearCompleted()
            else -> return super.onOptionsItemSelected(item)
        }

        return true
    }

    fun addItem() {
        viewModel.addItem(binding.newItemName.text.toString())
        binding.newItemName.text.clear()
    }
}
