package hu.t_bond.b_smarthome.ui.settings

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import hu.t_bond.b_smarthome.ConstantCache
import hu.t_bond.b_smarthome.R
import hu.t_bond.b_smarthome.Utilities.changeDayNightMode

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.settings)

        findPreference<Preference>(ConstantCache.PREF_THEME)?.apply {
            onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _: Preference?, newValue: Any? ->
                (newValue as? String)?.let { changeDayNightMode(it) }
                true
            }
        }
    }
}
