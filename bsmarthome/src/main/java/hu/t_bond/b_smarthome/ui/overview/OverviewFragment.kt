package hu.t_bond.b_smarthome.ui.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
import hu.t_bond.b_smarthome.R
import hu.t_bond.b_smarthome.model.adapters.ViewPagerAdapter
import hu.t_bond.b_smarthome.model.viewmodel.OverviewViewModel
import kotlinx.android.synthetic.main.fragment_overview.*

class OverviewFragment : Fragment() {
    private lateinit var tabAdapter: ViewPagerAdapter
    private val viewModel: OverviewViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        tabAdapter = ViewPagerAdapter(this)
        viewModel.getViews().observe(viewLifecycleOwner, Observer {
            tabAdapter.addAll(it)
            tabLayout.visibility = if (it.size <= 1) View.GONE else View.VISIBLE
        })

        return inflater.inflate(R.layout.fragment_overview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewPager.adapter = tabAdapter
        TabLayoutMediator(tabLayout, viewPager,
                TabConfigurationStrategy { tab: TabLayout.Tab, position: Int ->
                    tab.text = tabAdapter.getTitle(position)
                    tab.icon = tabAdapter.getIcon(requireContext(), position)
                }).attach()
    }
}
