package hu.t_bond.b_smarthome.ui.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import hu.t_bond.b_smarthome.R
import hu.t_bond.b_smarthome.model.adapters.CardAdapter
import kotlinx.android.synthetic.main.fragment_view.*
import hu.t_bond.homeassistant.model.View as ModelView

class ViewFragment : Fragment() {

    private lateinit var view: ModelView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        view = requireArguments().getSerializable(ARGUMENTS_VIEW)!! as ModelView
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cardRecyclerView.adapter = CardAdapter(this.view.cards)
    }

    companion object {
        private const val ARGUMENTS_VIEW = "view"

        fun newInstance(view: ModelView): ViewFragment {
            return ViewFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARGUMENTS_VIEW, view)
                }
            }
        }
    }
}
