package hu.t_bond.b_smarthome

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import hu.t_bond.homeassistant.HASSConnection
import hu.t_bond.homeassistant.listeners.ResponseListener
import hu.t_bond.homeassistant.model.Session
import hu.t_bond.homeassistant.requests.AuthorizeSession
import hu.t_bond.homeassistant.requests.GenerateLongLivedToken
import hu.t_bond.homeassistant.requests.IsConnected
import hu.t_bond.homeassistant.requests.RevokeRefreshToken
import kotlinx.android.synthetic.main.activity_authentication.*
import java.util.regex.Pattern

class AuthenticationActivity : AppCompatActivity() {

    private val inputChangeListener = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        override fun afterTextChanged(s: Editable?) {
            validateForm()
        }
    }

    private val connectionStateListener = object : ResponseListener<Boolean>() {
        override fun onResult(result: Boolean) {
            if (result) {
                onLogin()
            } else {
                ConnectionHandler.currentConnection.disconnect()
                progressLayout.visibility = View.GONE
                setInputAvailability(true)
                Snackbar.make(authenticationLayout, this@AuthenticationActivity.getString(R.string.token_is_invalid), Snackbar.LENGTH_LONG).show()
            }
        }
    }

    private val tokenGenerationResult = object : ResponseListener<Session?>() {
        override fun onResult(result: Session?) {
            if (result != null) {
                val oldSession = ConnectionHandler.currentConnection.session
                ConnectionHandler.currentConnection.disconnect()
                createConnection(result, ConnectionHandler.currentConnection.baseURL)
                ConnectionHandler.currentConnection.addRequest(RevokeRefreshToken(oldSession))
                onLogin()
            } else runOnUiThread {
                Toast.makeText(this@AuthenticationActivity, this@AuthenticationActivity.getString(R.string.token_creation_error), Toast.LENGTH_LONG).show()
                onLogin()
            }
        }
    }

    private val onRequestToken = object : ResponseListener<Session?>() {
        override fun onResult(result: Session?) {
            if(result == null) {
                ConnectionHandler.currentConnection.disconnect()
                progressLayout.visibility = View.GONE
                setInputAvailability(true)
                Snackbar.make(authenticationLayout, this@AuthenticationActivity.getString(R.string.login_failed), Snackbar.LENGTH_LONG).show()
                return
            }

            createConnection(result, tvUrl.text.toString())
            saveConnection()
            AlertDialog.Builder(this@AuthenticationActivity)
                    .setMessage(R.string.create_long_lived_access_token)
                    .setTitle(R.string.long_lived_access_token_permission_title)
                    .setPositiveButton(android.R.string.yes) { _: DialogInterface?, _: Int ->
                        val longLivedToken = GenerateLongLivedToken(ConnectionHandler.currentConnection.session.clientID, CLIENT_NAME, CLIENT_ICON)
                        longLivedToken.responseListener = tokenGenerationResult
                        ConnectionHandler.currentConnection.addRequest(longLivedToken)
                    }
                    .setNegativeButton(android.R.string.no) { _: DialogInterface?, _: Int ->
                        onLogin()
                    }.create().show()
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)

        @SuppressLint("SetTextI18n")
        if (BuildConfig.DEBUG)
            tvUrl.setText("https://demo.nt.t-bond.hu/")

        validateForm()

        @SuppressLint("SetJavaScriptEnabled")
        webViewForToken.settings.javaScriptEnabled = true

        tvUrl.addTextChangedListener(inputChangeListener)
        tvToken.addTextChangedListener(inputChangeListener)

        btnOpenInBrowser.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(tvUrl.text.toString())))
        }

        btnLogin.setOnClickListener {
            val baseURL = tvUrl.text.toString()
            val longLivedToken = tvToken.text.toString()
            if (longLivedToken.isEmpty()) { // Try user based login
                webViewForToken.loadUrl(baseURL + AUTHORIZATION_PATH)
                webViewForToken.visibility = View.VISIBLE
            } else { // Try login with long-lived auth token
                createConnection(Session(CLIENT_ID, longLivedToken, GenerateLongLivedToken.DEFAULT_EXPIRY), tvUrl.text.toString())
                progressLayout.visibility = View.VISIBLE
                setInputAvailability(false)

                val isConnected = IsConnected()
                isConnected.responseListener = connectionStateListener
                ConnectionHandler.currentConnection.addRequest(isConnected)
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        val data: Uri? = intent?.data

        if(AUTHORIZATION_URL.startsWith("${data?.scheme}://${data?.host}")) {
            val authorizationCode = data?.getQueryParameter("code")
            if(authorizationCode != null)
                onAuthorizationCode(authorizationCode)
        }
    }

    private fun validateForm() {
        var isURLValid = Pattern.matches(urlPattern, tvUrl.text.toString())
        val isTokenValid = tvToken.text?.isEmpty() ?: true
                || Pattern.matches(tokenPattern, tvToken.text.toString())

        btnOpenInBrowser.isEnabled = isURLValid
        btnLogin.isEnabled = isURLValid && isTokenValid
        isURLValid = tvUrl.text?.isEmpty() ?: true
                || isURLValid
        textInputLayoutUrl.isErrorEnabled = !isURLValid
        textInputLayoutToken.isErrorEnabled = !isTokenValid

        if (!isURLValid)
            textInputLayoutUrl.error = getString(R.string.url_wrong_format)

        if (!isTokenValid)
            textInputLayoutToken.error = getString(R.string.token_wrong_format)
    }

    override fun onBackPressed() {
        if (webViewForToken.visibility != View.GONE && webViewForToken.canGoBack())
            webViewForToken.goBack()
        else if (webViewForToken.visibility != View.GONE)
            webViewForToken.visibility = View.GONE
        else super.onBackPressed()
    }

    private fun onAuthorizationCode(authorizationCode: String) {
        progressLayout.visibility = View.VISIBLE
        webViewForToken.visibility = View.GONE
        createConnection(Session(CLIENT_ID, authorizationCode, 1), tvUrl.text.toString())
        val request = AuthorizeSession(CLIENT_ID, authorizationCode)
        request.responseListener = onRequestToken
        ConnectionHandler.currentConnection.addRequest(request)
    }


    private fun onLogin() {
        saveConnection()

        startActivity(Intent(applicationContext, MainActivity::class.java))
        finish()
    }

    private fun saveConnection() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(this@AuthenticationActivity).edit()
        preferences.putString(HASSConnection.SP_BASE_URL, ConnectionHandler.currentConnection.baseURL)
        preferences.putSerializable(ConstantCache.SESSION, ConnectionHandler.currentConnection.session)
        preferences.apply()
    }

    private fun createConnection(session: Session, baseURL: String) {
        ConnectionHandler.currentConnection = HASSConnection(session, baseURL)
    }

    private fun setInputAvailability(enabled: Boolean) {
        tvUrl.isEnabled = enabled
        tvToken.isEnabled = enabled
        btnLogin.isEnabled = enabled
        btnOpenInBrowser.isEnabled = enabled
    }

    companion object {
        private const val urlPattern = "^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"
        private const val tokenPattern = "[A-Za-z0-9\\-._~+/]+=*"

        private const val CLIENT_NAME = "B-Smart Home"
        private const val CLIENT_ICON = "mdi:android"

        private const val CLIENT_ID = "https://home-assistant"
        private const val AUTHORIZATION_URL = "${CLIENT_ID}/authorization"
        private const val AUTHORIZATION_PATH = "/auth/authorize?client_id=${CLIENT_ID}&redirect_uri=${AUTHORIZATION_URL}"
    }
}
