package hu.t_bond.b_smarthome

import android.content.Context

object ConstantCache {
    const val SESSION = "Session"

    lateinit var DISABLE_COLLAPSE: String
        private set

    lateinit var SHOW_FAB: String
        private set

    lateinit var TITLE: String
        private set

    lateinit var THEME_AUTO: String
        private set

    lateinit var THEME_NIGHT: String
        private set

    lateinit var THEME_LIGHT: String
        private set

    lateinit var PREF_THEME: String
        private set

    fun init(context: Context) {
        DISABLE_COLLAPSE = context.getString(R.string.disable_collapse)
        THEME_AUTO = context.getString(R.string.val_theme_auto)
        THEME_NIGHT = context.getString(R.string.val_theme_night)
        THEME_LIGHT = context.getString(R.string.val_theme_light)
        PREF_THEME = context.getString(R.string.pref_theme)
        SHOW_FAB = context.getString(R.string.show_fab)
        TITLE = context.getString(R.string.title)
    }
}
