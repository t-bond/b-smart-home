package hu.t_bond.b_smarthome.model.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hu.t_bond.b_smarthome.model.cards.viewholder.CardViewHolder
import hu.t_bond.homeassistant.model.cards.CARD_TYPE
import hu.t_bond.homeassistant.model.cards.Card

class CardAdapter(private var cards: List<Card>, private val inHorizontalView: Boolean = false) : RecyclerView.Adapter<CardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        return CardViewHolder.getCardViewHolder(LayoutInflater.from(parent.context), parent, false, enumValues<CARD_TYPE>()[viewType], inHorizontalView)
    }

    override fun getItemViewType(position: Int): Int {
        return cards[position].cardType.ordinal
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        holder.bind(cards[position])
    }

    override fun getItemCount() = cards.size
}
