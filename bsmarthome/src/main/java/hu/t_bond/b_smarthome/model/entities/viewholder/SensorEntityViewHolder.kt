package hu.t_bond.b_smarthome.model.entities.viewholder

import hu.t_bond.b_smarthome.databinding.ItemSensorBinding
import hu.t_bond.homeassistant.model.entities.Entity
import hu.t_bond.homeassistant.model.entities.SensorEntity

class SensorEntityViewHolder(private val binding: ItemSensorBinding) : EntityViewHolder(binding.root) {

    override fun bind(entity: Entity) {
        if (entity is SensorEntity)
            binding.sensor = entity
    }
}
