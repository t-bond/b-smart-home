package hu.t_bond.b_smarthome.model.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hu.t_bond.b_smarthome.R
import hu.t_bond.b_smarthome.databinding.ItemSensorBinding
import hu.t_bond.b_smarthome.databinding.ItemSwitchBinding
import hu.t_bond.b_smarthome.model.entities.viewholder.EntityViewHolder
import hu.t_bond.b_smarthome.model.entities.viewholder.SensorEntityViewHolder
import hu.t_bond.b_smarthome.model.entities.viewholder.SwitchEntityViewHolder
import hu.t_bond.b_smarthome.model.entities.viewholder.UnknownEntityViewHolder
import hu.t_bond.homeassistant.model.entities.ENTITY_TYPE
import hu.t_bond.homeassistant.model.entities.Entity

class EntitiesAdapter(private var entities: List<Entity>) : RecyclerView.Adapter<EntityViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntityViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (enumValues<ENTITY_TYPE>()[viewType]) {
            ENTITY_TYPE.SENSOR -> SensorEntityViewHolder(ItemSensorBinding.inflate(inflater, parent, false))
            ENTITY_TYPE.SWITCH,
            ENTITY_TYPE.AUTOMATION,
            ENTITY_TYPE.INPUT_BOOLEAN -> SwitchEntityViewHolder(ItemSwitchBinding.inflate(inflater, parent, false))
            else -> UnknownEntityViewHolder(inflater.inflate(R.layout.item_unknown, parent, false))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return entities[position].entityType.ordinal
    }

    override fun onBindViewHolder(holder: EntityViewHolder, position: Int) {
        holder.bind(entities[position])
    }

    override fun getItemCount() = entities.size
}
