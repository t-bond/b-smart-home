package hu.t_bond.b_smarthome.model.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import hu.t_bond.b_smarthome.databinding.ItemShoppingListBinding
import hu.t_bond.b_smarthome.model.entities.viewholder.ShoppingListItemViewHolder
import hu.t_bond.homeassistant.model.ShoppingListItem

class ShoppingListAdapter(private var shoppingListItems: MutableLiveData<List<ShoppingListItem>>) : RecyclerView.Adapter<ShoppingListItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingListItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ShoppingListItemViewHolder(ItemShoppingListBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ShoppingListItemViewHolder, position: Int) {
        holder.bind(shoppingListItems.value!![position])
    }

    override fun getItemCount() = shoppingListItems.value?.size ?: 0
}
