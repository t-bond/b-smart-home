package hu.t_bond.b_smarthome.model.entities.viewholder

import hu.t_bond.b_smarthome.databinding.ItemSwitchBinding
import hu.t_bond.homeassistant.model.entities.Entity

class SwitchEntityViewHolder(private val binding: ItemSwitchBinding) : EntityViewHolder(binding.root) {

    override fun bind(entity: Entity) {
        binding.entity = entity
    }
}
