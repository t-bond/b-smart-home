package hu.t_bond.b_smarthome.model.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hu.t_bond.b_smarthome.ConnectionHandler
import hu.t_bond.homeassistant.listeners.ResponseListener
import hu.t_bond.homeassistant.model.Component
import hu.t_bond.homeassistant.model.Config
import hu.t_bond.homeassistant.requests.GetConfig

class MainViewModel : ViewModel() {

    private var fetchRunning = false
    private val accountName = MutableLiveData<String>()
    private val components = MutableLiveData<List<Component>>()
    private lateinit var config: Config
    private val onConfigFetch: ResponseListener<Config?> = object : ResponseListener<Config?>() {
        override fun onResult(result: Config?) {
            fetchRunning = false
            if (result == null)
                return

            config = result
            accountName.postValue(config.location_name)
            components.postValue(config.components)
        }
    }

    private val getConfig = GetConfig().apply {
        responseListener = onConfigFetch
    }

    fun getCurrentAccountName(): LiveData<String> {
        fetchServerConfiguration()
        return accountName
    }

    fun getComponents(): LiveData<List<Component>> {
        fetchServerConfiguration()
        return components
    }

    init {
        ConnectionHandler.currentConnection.apply {
            entityStore.update()
            entityStore.automaticUpdates = true
        }
    }

    private fun fetchServerConfiguration() {
        if (fetchRunning)
            return

        fetchRunning = true
        ConnectionHandler.currentConnection.addRequest(getConfig)
    }
}
