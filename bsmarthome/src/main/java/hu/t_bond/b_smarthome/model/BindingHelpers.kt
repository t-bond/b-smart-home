package hu.t_bond.b_smarthome.model

import androidx.databinding.BindingAdapter
import com.mikepenz.iconics.typeface.IIcon
import com.mikepenz.iconics.utils.sizePx
import com.mikepenz.iconics.view.IconicsImageView

@BindingAdapter("iiv_icon")
fun <T> setIconicsImageViewIcon(imageView: IconicsImageView, newIcon: IIcon) {
    imageView.icon?.icon = newIcon
}

@BindingAdapter("iiv_sizePx")
fun <T> setIconicsImageViewHeight(imageView: IconicsImageView, newHeight: Int) {
    imageView.icon?.sizePx = newHeight
}
