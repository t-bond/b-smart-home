package hu.t_bond.b_smarthome.model.entities.viewholder

import android.view.View
import hu.t_bond.homeassistant.model.entities.Entity

class UnknownEntityViewHolder(root: View) : EntityViewHolder(root) {
    override fun bind(entity: Entity) {}
}
