package hu.t_bond.b_smarthome.model.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hu.t_bond.b_smarthome.ConnectionHandler
import hu.t_bond.homeassistant.listeners.ResponseListener
import hu.t_bond.homeassistant.model.ShoppingListItem
import hu.t_bond.homeassistant.requests.AddShoppingListItem
import hu.t_bond.homeassistant.requests.ClearCompletedShoppingListItems
import hu.t_bond.homeassistant.requests.ListShoppingListItems

class ShoppingListViewModel : ViewModel() {
    private val items = MutableLiveData<List<ShoppingListItem>>()
    private val onListUpdate = object : ResponseListener<List<ShoppingListItem>>() {
        override fun onResult(result: List<ShoppingListItem>) {
            items.postValue(result)
        }
    }

    private val onNewItem = object : ResponseListener<ShoppingListItem>() {
        override fun onResult(result: ShoppingListItem) {
            items.postValue((items.value?.toMutableList() ?: mutableListOf()).apply {
                add(0, result)
            })
        }
    }

    private val onClearComplete = object : ResponseListener<Boolean>() {
        override fun onResult(result: Boolean) {
            if (result)
                items.postValue((items.value?.toMutableList() ?: mutableListOf()).apply {
                    removeAll { it.complete }
                })
        }
    }

    private val updateRequest = ListShoppingListItems().apply {
        responseListener = onListUpdate
    }

    private val clearCompleted = ClearCompletedShoppingListItems().apply {
        responseListener = onClearComplete
    }

    init {
        ConnectionHandler.currentConnection.addRequest(updateRequest)
    }

    fun getItems(): MutableLiveData<List<ShoppingListItem>> {
        return items
    }

    fun addItem(itemName: String) {
        ConnectionHandler.currentConnection.addRequest(AddShoppingListItem(itemName).apply { responseListener = onNewItem })
    }

    fun clearCompleted() {
        ConnectionHandler.currentConnection.addRequest(clearCompleted)
    }

}
