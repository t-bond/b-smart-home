package hu.t_bond.b_smarthome.model.cards.viewholder

import hu.t_bond.b_smarthome.databinding.CardVerticalStackBinding
import hu.t_bond.b_smarthome.model.adapters.CardAdapter
import hu.t_bond.homeassistant.model.cards.Card
import hu.t_bond.homeassistant.model.cards.VerticalStackCard

class VerticalStackCardViewHolder(private val binding: CardVerticalStackBinding, inHorizontalView: Boolean = false) : CardViewHolder(binding.root, inHorizontalView) {

    override fun bind(card: Card) {
        if (card is VerticalStackCard) {
            binding.card = card
            binding.cardsList.adapter = CardAdapter(card.cards)
        }
        binding.executePendingBindings()
    }
}
