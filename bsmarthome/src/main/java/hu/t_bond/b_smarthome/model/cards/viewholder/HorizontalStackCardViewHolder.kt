package hu.t_bond.b_smarthome.model.cards.viewholder

import hu.t_bond.b_smarthome.databinding.CardHorizontalStackBinding
import hu.t_bond.b_smarthome.model.adapters.CardAdapter
import hu.t_bond.homeassistant.model.cards.Card
import hu.t_bond.homeassistant.model.cards.HorizontalStackCard

class HorizontalStackCardViewHolder(private val binding: CardHorizontalStackBinding, inHorizontalView: Boolean = false) : CardViewHolder(binding.root, inHorizontalView) {

    override fun bind(card: Card) {
        if (card is HorizontalStackCard) {
            binding.card = card
            binding.cardsList.adapter = CardAdapter(card.cards, true)
        }

        binding.executePendingBindings()
    }
}
