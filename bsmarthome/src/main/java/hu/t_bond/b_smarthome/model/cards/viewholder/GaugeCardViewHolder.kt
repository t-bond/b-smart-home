package hu.t_bond.b_smarthome.model.cards.viewholder

import android.graphics.Color
import hu.t_bond.b_smarthome.databinding.CardGaugeBinding
import hu.t_bond.homeassistant.model.cards.Card
import hu.t_bond.homeassistant.model.cards.GaugeCard

class GaugeCardViewHolder(private val binding: CardGaugeBinding, inHorizontalView: Boolean = false) : CardViewHolder(binding.root, inHorizontalView) {

    override fun bind(card: Card) {
        if (card is GaugeCard) {
            binding.card = card
            binding.state.max = card.max.toFloat()
            binding.state.progress = card.entity.state.toFloat() - card.min

            val value = binding.state.progress.toInt()
            when {
                value >= card.severityGreen ?: (card.max + 1) -> binding.state.circleProgressColor = Color.GREEN
                value >= card.severityYellow ?: (card.max + 1) -> binding.state.circleProgressColor = Color.YELLOW
                value >= card.severityRed ?: (card.max + 1) -> binding.state.circleProgressColor = Color.RED
            }

        }
        binding.executePendingBindings()
    }
}
