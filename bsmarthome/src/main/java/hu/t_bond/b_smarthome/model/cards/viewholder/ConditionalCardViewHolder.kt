package hu.t_bond.b_smarthome.model.cards.viewholder

import android.view.LayoutInflater
import hu.t_bond.b_smarthome.databinding.CardConditionalBinding
import hu.t_bond.homeassistant.model.cards.Card
import hu.t_bond.homeassistant.model.cards.ConditionalCard


class ConditionalCardViewHolder(private val binding: CardConditionalBinding, inHorizontalView: Boolean = false) : CardViewHolder(binding.root, inHorizontalView) {

    private var viewHolder: CardViewHolder? = null

    override fun bind(card: Card) {
        if (card is ConditionalCard) {
            binding.card = card

            val parent = binding.cardHolder
            viewHolder = getCardViewHolder(LayoutInflater.from(parent.context), parent, true, card.card.cardType, inHorizontalView)
            viewHolder?.bind(card.card)
            updateVisibility()
        }
        binding.executePendingBindings()
    }

    private fun updateVisibility() {
        if (binding.card?.conditionsMet() == true)
            updateSize(itemView)
        else
            itemView.layoutParams.apply {
                height = 0
                width = 0
            }
    }
}
