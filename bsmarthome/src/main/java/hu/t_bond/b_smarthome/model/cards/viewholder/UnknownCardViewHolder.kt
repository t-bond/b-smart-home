package hu.t_bond.b_smarthome.model.cards.viewholder

import android.view.View
import hu.t_bond.homeassistant.model.cards.Card

class UnknownCardViewHolder(root: View, inHorizontalView: Boolean = false) : CardViewHolder(root, inHorizontalView) {
    override fun bind(card: Card) {}
}
