package hu.t_bond.b_smarthome.model.cards.viewholder

import hu.t_bond.b_smarthome.databinding.CardLightBinding
import hu.t_bond.homeassistant.model.cards.Card
import hu.t_bond.homeassistant.model.cards.LightCard

class LightCardViewHolder(private val binding: CardLightBinding, inHorizontalView: Boolean = false) : CardViewHolder(binding.root, inHorizontalView) {

    override fun bind(card: Card) {
        if (card is LightCard)
            binding.card = card
        binding.executePendingBindings()
    }
}
