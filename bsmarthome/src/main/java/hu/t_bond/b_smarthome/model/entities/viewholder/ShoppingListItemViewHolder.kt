package hu.t_bond.b_smarthome.model.entities.viewholder

import android.graphics.Paint
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hu.t_bond.b_smarthome.ConnectionHandler
import hu.t_bond.b_smarthome.databinding.ItemShoppingListBinding
import hu.t_bond.homeassistant.model.ShoppingListItem
import hu.t_bond.homeassistant.requests.UpdateShoppingListItem

class ShoppingListItemViewHolder(private val binding: ItemShoppingListBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ShoppingListItem) {
        binding.item = item

        binding.itemCompleted.setOnClickListener {
            updateStrikeThru()
            updateItem()
        }

        binding.itemName.setOnEditorActionListener { _: TextView, actionID: Int, _: KeyEvent? ->
            if (actionID == EditorInfo.IME_ACTION_DONE) {
                updateItem()
                return@setOnEditorActionListener true
            }
            false
        }

        updateStrikeThru()
    }

    private fun updateItem() {
        binding.item?.run {
            ConnectionHandler.currentConnection.addRequest(UpdateShoppingListItem(this))
        }

    }

    private fun updateStrikeThru() {
        binding.itemName.paintFlags =
                if (binding.item?.complete == true)
                    binding.itemName.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                else
                    binding.itemName.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
    }
}
