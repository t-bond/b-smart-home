package hu.t_bond.b_smarthome.model.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hu.t_bond.b_smarthome.ConnectionHandler
import hu.t_bond.homeassistant.listeners.ResponseListener
import hu.t_bond.homeassistant.model.LovelaceConfig
import hu.t_bond.homeassistant.model.View
import hu.t_bond.homeassistant.requests.GetLovelaceConfig

class OverviewViewModel : ViewModel() {

    private val hasConfig = MutableLiveData<Boolean>()
    private val views = MutableLiveData<List<View>>()
    private var request: GetLovelaceConfig? = null

    private val onLovelaceConfigData = object : ResponseListener<LovelaceConfig>() {
        override fun onResult(result: LovelaceConfig) {
            hasConfig.postValue(result.hasConfig)
            views.postValue(result.views)
            request = null
        }
    }

    fun hasConfig(): LiveData<Boolean> {
        if (hasConfig.value == null)
            updateLovelace()

        return hasConfig
    }

    fun getViews(): LiveData<List<View>> {
        if (views.value == null)
            updateLovelace()

        return views
    }

    fun updateLovelace() {
        if (request != null)
            return

        ConnectionHandler.currentConnection.apply {
            request = GetLovelaceConfig(entityStore).also {
                it.responseListener = onLovelaceConfigData
                addRequest(it)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        request?.responseListener = null
    }
}
