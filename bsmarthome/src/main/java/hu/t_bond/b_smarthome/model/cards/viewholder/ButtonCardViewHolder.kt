package hu.t_bond.b_smarthome.model.cards.viewholder

import hu.t_bond.b_smarthome.ConnectionHandler
import hu.t_bond.b_smarthome.databinding.CardButtonBinding
import hu.t_bond.homeassistant.model.cards.ButtonCard
import hu.t_bond.homeassistant.model.cards.Card
import hu.t_bond.homeassistant.requests.CallService
import hu.t_bond.homeassistant.services.ServiceToggle

class ButtonCardViewHolder(private val binding: CardButtonBinding, inHorizontalView: Boolean = false) : CardViewHolder(binding.root, inHorizontalView) {

    override fun onClick() {
        super.onClick()

        ConnectionHandler.currentConnection.addRequest(CallService(ServiceToggle(binding.card!!.entity)))
    }

    override fun bind(card: Card) {
        if (card is ButtonCard)
            binding.card = card
        binding.root.setOnClickListener { onClick() }
        binding.executePendingBindings()
    }
}
