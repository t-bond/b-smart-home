package hu.t_bond.b_smarthome.model.cards.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hu.t_bond.b_smarthome.R
import hu.t_bond.b_smarthome.databinding.*
import hu.t_bond.homeassistant.model.cards.CARD_TYPE
import hu.t_bond.homeassistant.model.cards.Card

abstract class CardViewHolder(itemView: View, protected val inHorizontalView: Boolean) : RecyclerView.ViewHolder(itemView) {
    init {
        updateSize(itemView)
    }

    abstract fun bind(card: Card)

    open fun onClick() {}

    protected fun updateSize(view: View) {
        view.layoutParams.apply {
            if (inHorizontalView) {
                width = ViewGroup.LayoutParams.WRAP_CONTENT
                height = ViewGroup.LayoutParams.MATCH_PARENT
            } else {
                width = ViewGroup.LayoutParams.MATCH_PARENT
                height = ViewGroup.LayoutParams.WRAP_CONTENT
            }
        }
    }


    companion object {
        fun getCardViewHolder(inflater: LayoutInflater, parent: ViewGroup, attach: Boolean, cardType: CARD_TYPE, inHorizontalView: Boolean = false) = when (cardType) {
            CARD_TYPE.ENTITIES -> EntitiesCardViewHolder(CardEntitiesBinding.inflate(inflater, parent, attach), inHorizontalView)
            CARD_TYPE.BUTTON -> ButtonCardViewHolder(CardButtonBinding.inflate(inflater, parent, attach), inHorizontalView)
            CARD_TYPE.VERTICAL_STACK -> VerticalStackCardViewHolder(CardVerticalStackBinding.inflate(inflater, parent, attach), inHorizontalView)
            CARD_TYPE.HORIZONTAL_STACK -> HorizontalStackCardViewHolder(CardHorizontalStackBinding.inflate(inflater, parent, attach), inHorizontalView)
            CARD_TYPE.CONDITIONAL -> ConditionalCardViewHolder(CardConditionalBinding.inflate(inflater, parent, attach), inHorizontalView)
            CARD_TYPE.LIGHT -> LightCardViewHolder(CardLightBinding.inflate(inflater, parent, attach), inHorizontalView)
            CARD_TYPE.GAUGE -> GaugeCardViewHolder(CardGaugeBinding.inflate(inflater, parent, attach), inHorizontalView)
            else -> UnknownCardViewHolder(inflater.inflate(R.layout.card_unknown, parent, attach), inHorizontalView)
        }
    }
}
