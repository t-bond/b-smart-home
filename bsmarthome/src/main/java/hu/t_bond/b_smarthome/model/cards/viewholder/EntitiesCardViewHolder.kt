package hu.t_bond.b_smarthome.model.cards.viewholder

import hu.t_bond.b_smarthome.databinding.CardEntitiesBinding
import hu.t_bond.b_smarthome.model.adapters.EntitiesAdapter
import hu.t_bond.homeassistant.model.cards.Card
import hu.t_bond.homeassistant.model.cards.EntitiesCard

class EntitiesCardViewHolder(private val binding: CardEntitiesBinding, inHorizontalView: Boolean = false) : CardViewHolder(binding.root, inHorizontalView) {

    override fun bind(card: Card) {
        if (card is EntitiesCard) {
            binding.card = card
            binding.entitiesList.adapter = EntitiesAdapter(card.entities)
        }
        binding.executePendingBindings()
    }
}
