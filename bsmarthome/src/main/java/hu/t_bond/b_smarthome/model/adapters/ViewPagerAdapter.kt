package hu.t_bond.b_smarthome.model.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.mikepenz.iconics.IconicsDrawable
import hu.t_bond.b_smarthome.Utilities.getIconFromString
import hu.t_bond.b_smarthome.ui.overview.ViewFragment
import hu.t_bond.homeassistant.model.View

class ViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private var views = emptyList<View>()

    fun addAll(views: List<View>) {
        this.views = views
        notifyDataSetChanged()
    }

    override fun createFragment(position: Int) = ViewFragment.newInstance(views[position])

    override fun getItemCount() = views.size

    fun getIcon(context: Context, position: Int) = IconicsDrawable(context, getIconFromString(views[position].icon))

    fun getTitle(position: Int) = views[position].title
}
