package hu.t_bond.b_smarthome.model.entities.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hu.t_bond.homeassistant.model.entities.Entity

abstract class EntityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(entity: Entity)
}
