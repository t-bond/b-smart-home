package hu.t_bond.b_smarthome

import android.content.SharedPreferences
import android.util.Base64
import androidx.appcompat.app.AppCompatDelegate
import com.mikepenz.iconics.typeface.library.community.material.CommunityMaterial.getIcon
import com.mikepenz.iconics.typeface.library.community.material.CommunityMaterial.mappingPrefix
import java.io.*
import java.util.*

object Utilities {

    @JvmStatic
    fun convertIconName(iconName: String?): String {
        val correctedIconName = if (iconName == null)
            "alert"
        else {
            iconName.split(":", limit = 2)[1]
                    .replace("-", "_")
                    .toLowerCase(Locale.ROOT)
        }
        return "${mappingPrefix}_$correctedIconName"
    }

    @JvmStatic
    fun getIconFromString(iconName: String?) = try {
        getIcon(convertIconName(iconName))
    } catch (ignored: Exception) {
        getIcon(convertIconName(null))
    }

    @JvmStatic
    fun changeDayNightMode(chosenTheme: String?) {
        AppCompatDelegate.setDefaultNightMode(when (chosenTheme) {
            ConstantCache.THEME_NIGHT -> AppCompatDelegate.MODE_NIGHT_YES
            ConstantCache.THEME_LIGHT -> AppCompatDelegate.MODE_NIGHT_NO
            else -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
        })
    }
}

fun SharedPreferences.Editor.putSerializable(preferenceName: String, serializable: Serializable) {
    val baos = ByteArrayOutputStream()
    ObjectOutputStream(baos).apply {
        writeObject(serializable)
        close()
    }

    putString(preferenceName, Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT))
}

fun <T : Serializable> SharedPreferences.getSerializable(preferenceName: String, defaultValue: T?): T? {
    val stringValue = getString(preferenceName, null) ?: return defaultValue

    var o: T?
    ObjectInputStream(ByteArrayInputStream(Base64.decode(stringValue, Base64.DEFAULT))).apply {
        @Suppress("UNCHECKED_CAST")
        o = readObject() as? T
        close()
    }

    return o
}
