package hu.t_bond.b_smarthome

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.appbar.AppBarLayout
import com.mikepenz.iconics.IconicsDrawable
import com.mikepenz.iconics.typeface.library.community.material.CommunityMaterial
import com.mikepenz.materialdrawer.holder.ImageHolder
import com.mikepenz.materialdrawer.holder.StringHolder
import com.mikepenz.materialdrawer.model.NavigationDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem
import com.mikepenz.materialdrawer.util.ExperimentalNavController
import com.mikepenz.materialdrawer.util.addStickyDrawerItems
import com.mikepenz.materialdrawer.util.setupWithNavController
import com.mikepenz.materialdrawer.widget.AccountHeaderView
import hu.t_bond.b_smarthome.model.viewmodel.MainViewModel
import hu.t_bond.homeassistant.model.Component
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var appBarConfiguration: AppBarConfiguration

    @ExperimentalNavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // val navController = findNavController(R.id.nav_host_fragment); // Blocked by: https://issuetracker.google.com/issues/143145612
        val navController = (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment).navController

        navController.setGraph(navController.graph, Bundle().apply {
            putString(ConstantCache.TITLE, getString(R.string.menu_overView)) /// TODO: Replace with default overview's title
        })

        appBarConfiguration = AppBarConfiguration(setOf(R.id.nav_overview, R.id.nav_shoppingList, R.id.nav_settings), drawer_layout)
        setSupportActionBar(toolbar)
        setupActionBarWithNavController(navController, appBarConfiguration)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)

        val defaultScrollFlags = (toolbar.layoutParams as AppBarLayout.LayoutParams).scrollFlags
        navController.addOnDestinationChangedListener { _, _, arguments ->
            if (arguments == null) return@addOnDestinationChangedListener

            if (arguments[ConstantCache.DISABLE_COLLAPSE] == true) {
                appBarLayout.setExpanded(true, true)
                (toolbar.layoutParams as AppBarLayout.LayoutParams).scrollFlags = 0
            } else
                (toolbar.layoutParams as AppBarLayout.LayoutParams).scrollFlags = defaultScrollFlags

            if (arguments[ConstantCache.SHOW_FAB] == true)
                FAB.show()
            else
                FAB.hide()
        }

        // Header
        AccountHeaderView(this).apply {
            attachToSliderView(nav_view)
            threeSmallProfileImages = true

            addProfiles(ProfileDrawerItem().apply {
                name = StringHolder(viewModel.getCurrentAccountName().value
                        ?: getString(R.string.loading_data))
                description = StringHolder(ConnectionHandler.currentConnection.baseURL)
                isNameShown = true
                identifier = 0
            }, ProfileSettingDrawerItem().apply {
                name = StringHolder(R.string.add_homeserver_title)
                description = StringHolder(R.string.add_homeserver_description)
                icon = ImageHolder(IconicsDrawable(context, CommunityMaterial.Icon2.cmd_home_plus))
                isIconTinted = true
                identifier = 42
            }
            )
            withSavedInstance(savedInstanceState)
        }

        // Settings
        nav_view.addStickyDrawerItems(NavigationDrawerItem(R.id.nav_settings, PrimaryDrawerItem().apply {
            name = StringHolder(R.string.menu_settings)
            icon = ImageHolder(IconicsDrawable(this@MainActivity, CommunityMaterial.Icon.cmd_cogs))
            isIconTinted = true
        }, null, null))

        nav_view.setSavedInstance(savedInstanceState)

        viewModel.getCurrentAccountName().observe(this, Observer {
            nav_view.accountHeader?.apply {
                activeProfile?.name = StringHolder("Teszt")
                updateHeaderAndList()
            }
        })

        viewModel.getComponents().observe(this, Observer {
            nav_view.itemAdapter.clear()
            /// TODO: Get the lovelace views, and populate by them
            nav_view.itemAdapter.add(
                    NavigationDrawerItem(R.id.nav_overview, PrimaryDrawerItem().apply {
                        name = StringHolder(R.string.menu_overView)
                        icon = ImageHolder(IconicsDrawable(this@MainActivity, CommunityMaterial.Icon2.cmd_view_dashboard))
                        isIconTinted = true
                    }, Bundle().apply {
                        putString(ConstantCache.TITLE, getString(R.string.menu_overView)) /// TODO: Replace with overview's title
                    }, null)
            )
            nav_view.selectedItemPosition = 0

            if (it.contains(Component.SHOPPING_LIST))
                @Suppress("EXPERIMENTAL_API_USAGE")
                this@MainActivity.nav_view.itemAdapter.add(2, NavigationDrawerItem(R.id.nav_shoppingList, PrimaryDrawerItem().apply {
                    name = StringHolder(R.string.menu_shopping_list)
                    icon = ImageHolder(IconicsDrawable(this@MainActivity, CommunityMaterial.Icon.cmd_cart))
                    isIconTinted = true
                }, null, null))
        })
    }

    override fun onSupportNavigateUp() = findNavController(R.id.nav_host_fragment).navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
}
