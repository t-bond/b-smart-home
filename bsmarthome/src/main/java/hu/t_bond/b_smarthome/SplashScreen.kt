package hu.t_bond.b_smarthome

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import hu.t_bond.homeassistant.HASSConnection
import hu.t_bond.homeassistant.model.Session

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ConstantCache.init(applicationContext)
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)

        // Restore the user selected theme
        Utilities.changeDayNightMode(preferences.getString(ConstantCache.PREF_THEME, ""))

        // Check logged in status
        var continueActivity: Class<*> = AuthenticationActivity::class.java
        if (preferences.contains(ConstantCache.SESSION)) {

            val baseURL = preferences.getString(HASSConnection.SP_BASE_URL, "")
            val restoredSession = preferences.getSerializable<Session>(ConstantCache.SESSION, null)
            if (!baseURL.isNullOrEmpty() && restoredSession != null) {
                continueActivity = MainActivity::class.java

                ConnectionHandler.currentConnection = HASSConnection(restoredSession, baseURL)
            }
        }

        startActivity(Intent(this, continueActivity))
        finish()
    }
}
